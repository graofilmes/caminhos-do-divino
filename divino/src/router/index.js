import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/:type?/:reference?',
    component: () => import('../views/Base.vue'),
    children: [{
      path: '',
      name: 'Home',
      components: {
        extra: () => import('../views/Extras.vue'),
        mainPath: () => import('../views/Home.vue')
      }
    }]
  },
  {
    path: '/divino/:path/:step/:type?/:reference?',
    component: () => import('../views/Base.vue'),
    children: [{
      path: '',
      name: 'Path',
      components: {
        extra: () => import('../views/Extras.vue'),
        mainPath: () => import('../views/Path.vue')
      }
    }]
  },
  {
    path: '/parada/:page/:path/:step/:type?/:reference?',
    component: () => import('../views/Base.vue'),
    children: [{
      path: '',
      name: 'Page',
      components: {
        extra: () => import('../views/Extras.vue'),
        mainPath: () => import('../views/Page.vue')
      }
    }]
  }
]

const router = new VueRouter({
  routes
})

export default router
