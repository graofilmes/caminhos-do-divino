import documents from '@/store/collectables/documents'
import gallerys from '@/store/collectables/gallerys'
import musics from '@/store/collectables/musics'
import panoramas from '@/store/collectables/panoramas'
import postcards from '@/store/collectables/postcards'
import videos from '@/store/collectables/videos'

const collectables = {
  postcards,
  musics,
  panoramas,
  gallerys,
  documents,
  videos
}

export default collectables
