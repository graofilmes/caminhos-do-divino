import afogado from '@/store/script/afogado'
import cavalhada from '@/store/script/cavalhada'
import congadas from '@/store/script/congadas'
import folia from '@/store/script/folia'
import igreja from '@/store/script/igreja'
import imperio from '@/store/script/imperio'
import pentecostes from '@/store/script/pentecostes'
import praca from '@/store/script/praca'

const script = {
  afogado,
  cavalhada,
  congadas,
  folia,
  igreja,
  imperio,
  pentecostes,
  praca
}

export default script
