import praca from '@/store/pages/praca'
import folia from '@/store/pages/folia'
import milagres from '@/store/pages/milagres'
import diversoes from '@/store/pages/diversoes'

const pages = {
  praca,
  folia,
  milagres,
  diversoes
}

export default pages
