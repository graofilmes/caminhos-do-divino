const milagres = {
  name: 'Milagres do Divino',
  content: '17',
  items: [
    {
      type: 'videos',
      src: '17a'
    },
    {
      type: 'videos',
      src: '17c'
    },
    {
      type: 'videos',
      src: '17d'
    },
    {
      type: 'videos',
      src: '17f'
    },
    {
      type: 'videos',
      src: '17g'
    },
    {
      type: 'videos',
      src: '17h'
    },
    {
      type: 'videos',
      src: '17k'
    },
    {
      type: 'videos',
      src: '17l'
    },
    {
      type: 'videos',
      src: '17n'
    }
  ]
}

export default milagres
