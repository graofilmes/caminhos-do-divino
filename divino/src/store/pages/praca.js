const praca = {
  name: 'A Praça',
  content: '30',
  slug: 'E a praça, cada vez mais cheia de gente, vira palco de todo tipo de diversão',
  items: [
    {
      type: 'videos',
      src: '29f'
    },
    {
      type: 'videos',
      src: '29h'
    },
    {
      type: 'videos',
      src: '29i'
    },
    {
      type: 'gallerys',
      src: '30e'
    },
    {
      type: 'videos',
      src: '29r'
    },
    {
      type: 'videos',
      src: '29u'
    },
    {
      type: 'postcards',
      src: '33h_1'
    },
    {
      type: 'videos',
      src: '29ze'
    },
    {
      type: 'videos',
      src: '29zh'
    },
    {
      type: 'videos',
      src: '29c'
    },
    {
      type: 'videos',
      src: '29d'
    },
    {
      type: 'gallerys',
      src: '30b'
    },
    {
      type: 'videos',
      src: '29g'
    },
    {
      type: 'videos',
      src: '29l'
    },
    {
      type: 'postcards',
      src: '33f'
    },
    {
      type: 'videos',
      src: '29m'
    },
    {
      type: 'videos',
      src: '29p'
    },
    {
      type: 'gallerys',
      src: '30g'
    },
    {
      type: 'videos',
      src: '40b3'
    },
    {
      type: 'videos',
      src: '29q'
    },
    {
      type: 'postcards',
      src: '33g'
    },
    {
      type: 'videos',
      src: '29s'
    },
    {
      type: 'videos',
      src: '29v'
    },
    {
      type: 'videos',
      src: '29w'
    },
    {
      type: 'gallerys',
      src: '30a'
    },
    {
      type: 'videos',
      src: '29z'
    },
    {
      type: 'videos',
      src: '29zb'
    },
    {
      type: 'postcards',
      src: '33h_2'
    },
    {
      type: 'videos',
      src: '40d'
    },
    {
      type: 'videos',
      src: '29zc'
    },
    {
      type: 'videos',
      src: '29zd'
    },
    {
      type: 'gallerys',
      src: '30f'
    },
    {
      type: 'postcards',
      src: '33h_3'
    },
    {
      type: 'videos',
      src: '29za'
    },
    {
      type: 'videos',
      src: '29j'
    }
  ]
}

export default praca
