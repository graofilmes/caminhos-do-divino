const folia = {
  name: 'Histórias da Folia',
  content: '03',
  items: [
    {
      type: 'videos',
      src: '03a'
    },
    {
      type: 'videos',
      src: '03b'
    },
    {
      type: 'videos',
      src: '03c1'
    },
    {
      type: 'videos',
      src: '03c2'
    },
    {
      type: 'videos',
      src: '03d'
    },
    {
      type: 'videos',
      src: '03f'
    },
    {
      type: 'videos',
      src: '03g1'
    },
    {
      type: 'videos',
      src: '03g2'
    },
    {
      type: 'videos',
      src: '03h'
    }
  ]
}

export default folia
