import Vue from 'vue'
import Vuex from 'vuex'

import VuexPersistence from 'vuex-persist'
import collectables from '@/store/modules/collectables'
import pages from '@/store/modules/pages'
import script from '@/store/modules/script'

Vue.use(Vuex)

const persist = new VuexPersistence({
  reducer: (state) => ({ trails: state.trails, viewed: state.viewed, collected: state.collected, tutorial: state.tutorial })
})

export default new Vuex.Store({
  state: {
    collectables,
    pages,
    script,
    trails: {
      Fe: [],
      Turismo: [],
      Conhecimento: []
    },
    viewed: {},
    collected: {},
    tutorial: {
      index: true,
      map: true,
      transition: true,
      praca: true
    }
  },
  mutations: {
    markSeen (state, pathObj) {
      if (!state.viewed[pathObj.path]) { Vue.set(state.viewed, pathObj.path, []) }
      if (!state.viewed[pathObj.path].includes(pathObj.step)) { state.viewed[pathObj.path].push(pathObj.step) }
    },
    markCollected (state, pathObj) {
      if (!state.collected[pathObj.cat]) { Vue.set(state.collected, pathObj.cat, []) }
      if (!state.collected[pathObj.cat].includes(pathObj.item)) { state.collected[pathObj.cat] = state.collected[pathObj.cat].concat(pathObj.item) }
    },
    markTrailed (state, pathObj) {
      for (const trail in pathObj.trail) {
        if (!state.trails[pathObj.trail[trail]].includes(pathObj.item)) { state.trails[pathObj.trail[trail]] = state.trails[pathObj.trail[trail]].concat(pathObj.item) }
      }
    },
    enableTutorial (state, item) {
      state.tutorial[item] = true
    },
    disableTutorial (state, item) {
      state.tutorial[item] = false
    }
  },
  actions: {
    seen ({ commit }, pathObj) {
      commit('markSeen', pathObj)
    },
    collected ({ commit }, pathObj) {
      commit('markCollected', pathObj)
    },
    trailed ({ commit }, pathObj) {
      commit('markTrailed', pathObj)
    },
    enabled ({ commit }, item) {
      commit('enableTutorial', item)
    },
    disabled ({ commit }, item) {
      commit('disableTutorial', item)
    }
  },
  plugins: [persist.plugin],
  modules: {
  },
  getters: {
    roteiro: (state) => state.script,
    coletaveis: (state) => state.collectables,
    viewed: (state) => state.viewed,
    collected: (state) => state.collected,
    trails: (state) => state.trails,
    tutorial: (state) => state.tutorial
  }
})
