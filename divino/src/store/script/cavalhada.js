const cavalhada = {
  name: 'Cavalhada',
  url: 'Cavalhada',
  text: `
  <p><span class="first-letter">É</span> tarde de sábado, véspera do Pentecostes. Vinte e quatro cavaleiros e seus cavalos ricamente adornados
  atravessam as ruas de São Luiz do Paraitinga. Eles vêm lá de Catuçaba para reencenar uma luta
  da época das cruzadas, em que os cristãos vencem os mouros e os convertem à sua fé.</p>
  <p>Com sua majestade, se dirigem ao campo onde fazem a população vibrar com suas manobras.</p>
  `,
  videos: [
    {
      src: '21',
      collectables: {
        panoramas: ['31g'],
        postcards: ['33l_1', '33l_2', '33l_3'],
        documents: ['41'],
        gallerys: ['50e'],
        videos: ['22']
      }
    }
  ]
}

export default cavalhada
