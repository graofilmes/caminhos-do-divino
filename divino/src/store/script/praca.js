const praca = {
  name: 'Praça',
  url: 'Praca',
  text: `
    <p><span class="first-letter">A</span> praça central de São Luiz do Paraitinga reúne pessoas que vêm dos mais diversos lugares
    para participar da festa do Divino e, por isso mesmo, transforma-se em palco de todo tipo de diversão.</p>
    <p>Caminhos que se cruzam e, entre danças, músicas e brincadeiras, reproduzem, no centro dessa
    pequena cidade, a mesma alegria bíblica do Pentecostes.</p>
  `,
  page: 'praca'
}

export default praca
