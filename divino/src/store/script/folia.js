const folia = {
  name: 'Folia',
  url: 'Folia',
  text: ` <p><span class="first-letter">A</span> festa do Divino termina no domingo de Pentecostes. Na semana seguinte, o festeiro já
      recomeça o percurso da Folia: vai de casa em casa, na cidade e na zona rural, levando uma
      bandeira de cor vermelha, com uma coroa e uma pomba que representa Deus.</p>
      <p>Vai acompanhado dos foliões e recebe contribuições para a realização da próxima festa.
      Mantém a fé em movimento durante o ano inteiro.</p>`,
  videos: [
    {
      src: '01',
      collectables: {
        musics: ['32b'],
        documents: ['46']
      }
    },
    {
      src: '02',
      collectables: {
        postcards: ['33p'],
        panoramas: ['31h'],
        gallerys: ['50a'],
        videos: ['40b4']
      }
    },
    {
      src: '04',
      collectables: {
        postcards: ['33q_1', '33q_2']
      },
      page: 'diversoes'
    },
    {
      src: '47',
      collectables: {
        postcards: ['33o_1', '33o_2'],
        musics: ['32c'],
        documents: ['39']
      },
      page: 'folia'
    }
  ]
}

export default folia
