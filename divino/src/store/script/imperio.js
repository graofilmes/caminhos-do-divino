const imperio = {
  name: 'Império',
  url: 'Imperio',
  text: `
    <p><span class="first-letter">O</span> Império do Divino é um lugar onde se acolhem os principais símbolos da festa: a imagem do
    Espírito Santo, a coroa, o cetro, a salva (bandeja onde se depositam esmolas) e, em especial, as
    dezenas de bandeiras dos devotos, cheias de fitas coloridas, chupetas e fotografias que
    representam os pedidos e as graças alcançadas por Sua intervenção.</p>
    <p>Esse espaço reproduz o cerimonial da corte de D. Dinis e D. Isabel na festa do Divino de 1296
    em Alenquer e é um ponto obrigatório de peregrinação.
    </p>
  `,
  videos: [
    {
      src: '10',
      collectables: {
        postcards: ['33a_1', '33a_2', '33a_3', '33r'],
        panoramas: ['31c'],
        documents: ['42'],
        gallerys: ['50c'],
        videos: ['40b2']
      }
    },
    {
      src: '13',
      collectables: {
        postcards: ['33b_1', '33b_2', '33b_3', '33c'],
        documents: ['14']
      }
    }
  ]
}

export default imperio
