const igreja = {
  name: 'Igreja',
  url: 'Igreja',
  text: `
    <p><span class="first-letter">A</span> festa do Divino começa a ser celebrada 40 dias após a Páscoa.</p>
    <p>Durante 9 noites consecutivas, os moradores de São Luiz do Paraitinga realizam uma rotina de
    fé: em procissão, levam suas bandeiras do Império para a Igreja e, depois da missa, da Igreja
    de volta para o Império. É uma maneira simbólica de rememorar a reunião dos apóstolos que
    antecedeu o dia de Pentecostes.</p>
    <p>Um novo traçado de silêncios e preces se acende no mapa da cidade.</p>
  `,
  videos: [
    {
      src: '16',
      collectables: {
        postcards: ['33d_1', '33d_2'],
        musics: ['32a'],
        gallerys: ['50b']
      }
    },
    {
      src: '18',
      collectables: {
        postcards: ['33e_2', '33e_1a', '33e_1b'],
        panoramas: ['31a'],
        musics: ['32f', '32e'],
        videos: ['20']
      }
    },
    {
      src: '11',
      collectables: {
        postcards: ['33s'],
        musics: ['32d'],
        panoramas: ['31b'],
        documents: ['12']
      },
      page: 'milagres'
    }
  ]
}

export default igreja
