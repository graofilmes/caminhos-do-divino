const afogado = {
  name: 'Afogado',
  url: 'Afogado',
  text: `
    <p><span class="first-letter">O</span> Afogado é a comida típica da festa do Divino.</p>
    <p>Sua preparação é fruto da união de muitas mãos: das doações feitas por toda a comunidade
    durante o período da Folia e do trabalho de um grupo enorme de voluntários durante a festa.</p>
    <p>Comida sagrada, à qual se atribui a cura de muitas doenças. É distribuído gratuitamente à
    população e é uma parada obrigatória no caminhar daqueles que frequentam a festa.</p>
  `,
  videos: [
    {
      src: '07',
      collectables: {
        postcards: ['33i'],
        documents: ['09']
      }
    },
    {
      src: '05',
      collectables: {
        postcards: ['33j'],
        documents: ['08']
      }
    },
    {
      src: '06',
      collectables: {
        postcards: ['33k'],
        panoramas: ['31d'],
        gallerys: ['50d']
      }
    }
  ]
}

export default afogado
