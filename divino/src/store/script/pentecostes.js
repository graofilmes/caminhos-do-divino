const pentecostes = {
  name: 'Procissão de Pentecostes',
  url: 'Procissao',
  text: `
    <p><span class="first-letter">A</span>o entardecer do domingo, último dia da festa do Divino, todos os caminhares se juntam num
    único cordão de fé movente: a grande procissão de Pentecostes. O momento em que todos os
    movimentos apontam para uma mesma direção. Um deslumbramento de cores, de diferenças, de encontros...</p>
    <p>Cada pessoa que está ali tem sua própria história, mas, juntos, vibram algo muito maior: a força
    do Espírito Santo. A força com a qual ela caminha e que também vai caminhar com ela durante
    todo o próximo ano.</p>
  `,
  videos: [
    {
      src: '45',
      collectables: {
        postcards: ['33n'],
        panoramas: ['31f'],
        videos: ['40a', '40b1', '19']
      }
    },
    {
      src: '15',
      collectables: {
        gallerys: ['50g']
      }
    }
  ]
}

export default pentecostes
