const congadas = {
  name: 'Congada',
  url: 'Congada',
  text: ` 
    <p><span class="first-letter">A</span> alvorada do domingo de Pentecostes é saudada 
    com batuques ritmados, que despertam o povo com animação. São Congadas, Moçambiques 
    e Maracatus que vêm reverenciar o Divino de diferentes maneiras.</p>
    <p>Durante o dia, a praça central vai ficando cada vez mais cheia de gente, de cores e de sons.</p>
  `,
  videos: [
    {
      src: '23',
      collectables: {
        panoramas: ['31e'],
        postcards: ['33m_1', '33m_2', '33m_3', '33m_4'],
        gallerys: ['25'],
        videos: ['40c', '40e']
      }
    },
    {
      src: '24',
      collectables: {
        gallerys: ['50f'],
        videos: ['24b']
      }
    }
  ]
}

export default congadas
