const makeClickEvent = function (nodeName) {
  return () => {
    var event = new CustomEvent('nodeClick', { detail: nodeName })
    document.dispatchEvent(event)
  }
}

const panoramas = {
  name: 'Fotos 360º',
  url: '360',
  description: `
    <p>Conforme vai navegando pelo site, você vai ganhando acesso a
    fotos 360 graus de cada um dos ambientes da festa.</p>
    <p>As fotos 360 graus vão sendo desbloqueadas à medida que você tiver assistido
    aos vídeos dos locais onde cada uma delas foi feita.</p>
  `,
  color: '#FFB620',
  items: {
    '31h': {
      name: 'Pouso do Divino',
      spots: [
        { pitch: 4, yaw: -101, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('Parentes e vizinhos vêm celebrar o pouso da bandeira') },
        { pitch: -20, yaw: 50, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('O Afogado também é a comida típica dos pousos do Divino') },
        { pitch: 10, yaw: 175, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('A festa no Pouso pode reunir mais de 500 pessoas numa única noite') },
        { pitch: 8, yaw: 28, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('Voluntários trabalham o dia todo pra servir a janta do Divino') }
      ]
    },
    '31a': {
      name: 'Novena',
      spots: [
        { pitch: 0, yaw: -83, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('Cada fiel carrega a sua bandeira') },
        { pitch: -15, yaw: -110, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('O povo que saiu da Igreja em procissão se dirige ao Império') },
        { pitch: 15, yaw: 112, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('O centro histórico da cidade é o palco dessa celebração') },
        { pitch: 1, yaw: 71, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('O ritual é chamado de novena, porque se repete durante 9 noites') }
      ]
    },
    '31b': {
      name: 'Igreja',
      spots: [
        { pitch: -5, yaw: 20, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('A Igreja tem capacidade para 450 pessoas sentadas') },
        { pitch: 2.3, yaw: -39.7, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('Cada fiel traz a sua bandeira pra missa nos 9 dias da novena') },
        { pitch: 34, yaw: -7, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('A Igreja foi totalmente destruída em janeiro de 2010 e reinaugurada em maio de 2014') },
        { pitch: 6, yaw: -3.2, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('No dia de Pentecostes, é o Bispo quem celebra a missa na cidade') }
      ]
    },
    '31d': {
      name: 'Mercado',
      spots: [
        { pitch: -20, yaw: 35, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('10 toneladas de alimentos são servidas durante a festa') },
        { pitch: 7, yaw: -125, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('50 voluntários trabalham incansavelmente pra produzir o Afogado') },
        { pitch: -5, yaw: 88.5, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('Cerca de 5.000 pessoas recebem gratuitamente o Afogado a cada ano') },
        { pitch: 0, yaw: -5, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('As instalações são provisórias, montadas só para os dias da festa') }
      ]
    },
    '31g': {
      name: 'Cavalhada',
      spots: [
        { pitch: 8, yaw: 105, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('Sr. Lauro de Castro Farias interpreta o rei cristão Carlos Magno') },
        { pitch: 10, yaw: 47, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('Sr. Renô Martins interpreta o rei mouro Marcílio') },
        { pitch: 10, yaw: -55, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('Os palhaços ajudam na organização e na segurança da apresentação') },
        { pitch: 8, yaw: -152, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('São 12 cavaleiros cristãos e 12 mouros com idades entre 8 e 83 anos') }
      ]
    },
    '31c': {
      name: 'Império',
      spots: [
        { pitch: 23, yaw: 11, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('A pomba é um símbolo do Divino, porque apareceu no batizado de Jesus') },
        { pitch: 27, yaw: -31.5, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('A bandeira do festeiro sai com apenas 7 fitas e vai ganhando adornos durante a esmolação') },
        { pitch: 20, yaw: 155, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('O vermelho é a cor associada ao Divino por ele ter descido em formato de línguas de fogo') },
        { pitch: 30, yaw: -120, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('Voluntários se revezam para atender a população no Império') },
        { pitch: 0, yaw: 7, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('A coroa foi doada para a paróquia em 1875') }
      ]
    },
    '31f': {
      name: 'Procissão de Pentecostes',
      spots: [
        { pitch: -15, yaw: -10, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('Vários andores acompanham a procissão') },
        { pitch: -10, yaw: 100, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('Pessoas vindas da zona rural e de várias cidades vizinhas acompanham a procissão de Pentecostes') },
        { pitch: -3, yaw: -85, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('Os festeiros desse ano seguram a bandeira. Os eleitos do próximo ano, seguram a coroa do Divino') },
        { pitch: -3, yaw: 50, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('Os Foliões do Divino e a Banda da cidade têm destaque na procissão') }
      ]
    },
    '31e': {
      name: 'Congada',
      spots: [
        { pitch: 5, yaw: -26.8, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('Vários grupos vêm de outros municípios para honrar o Divino') },
        { pitch: 10, yaw: 110, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('Instrumentos de percussão lembram suas origens africanas') },
        { pitch: 20, yaw: 0, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('Descendentes de africanos e edifícios tombados relembram a história dessa região do Estado') },
        { pitch: 14, yaw: -91.5, cssClass: 'bullet', clickHandlerFunc: makeClickEvent('Congadas, Maracatus e Moçambiques atraem turistas que movimentam a economia da cidade') }
      ]
    }
  }
}

export default panoramas
