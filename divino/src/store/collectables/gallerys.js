const gallerys = {
  name: 'Galerias de Fotos',
  url: 'Galerias',
  description: `
    <p>Conforme vai navegando pelo site, você vai ganhando
    acesso a galerias de fotos com imagens recentes e antigas da festa.</p>
    <p>As galerias de fotos vão sendo desbloqueadas à medida que você tiver
    acessado os mesmos conteúdos ao longo da navegação e foram colocadas
    aqui apenas para facilitar sua localização futura.</p>
  `,
  color: '#008FDE',
  items: {
    '50a': {
      name: 'A folia antigamente',
      trail: ['Turismo'],
      alt: [
        `
          Grupo de 11 pessoas, todos em pé, em frente a uma casa cor de rosa na zona rural
          posam para a foto logo após a visita do Divino na Folia de 1994. A pessoa que está no centro
          do grupo, segura a bandeira do Divino. A parte superior e a lateral direita da imagem estão
          bastante estragadas e manchadas, porque ela faz parte de um acervo que foi molhado durante
          a enchente que aconteceu em São Luiz do Paraitinga em janeiro de 2010.
        `,
        `
          Um grupo de 10 pessoas está à direita, posando para a fotografia, em frente a uma
          casa branca na zona rural logo após a visita do Divino na Folia de 1994. 3 deles estão sentados,
          à frente, e os outros estão em pé, formando uma segunda fileira. Do lado esquerdo da foto, 3
          cavalos arreados estão amarrados a uma cerca de madeira próximos a um comedouro. A
          lateral direita da imagem está bastante estragada e manchada, porque ela faz parte de um
          acervo que foi molhado durante a enchente que aconteceu em São Luiz do Paraitinga em
          janeiro de 2010.
        `,
        `
          Grupo de cerca de 15 pessoas, todos em pé, em frente a uma casa branca na zona
          rural posam para a foto logo após a visita do Divino na Folia de 1994. A pessoa que está no
          centro do grupo, segura a bandeira do Divino. À esquerda da imagem vê-se o telhado do
          curral. As bordas da imagem estão bastante estragadas e manchadas, porque ela faz parte de
          um acervo que foi molhado durante a enchente que aconteceu em São Luiz do Paraitinga em
          janeiro de 2010.
        `,
        `
          Um grupo de 15 pessoas posa para a fotografia. Estão todos de pé. O homem que
          está no centro do grupo, segura a bandeira do Divino. No meio do grupo, mais para a direita, o
          padre participa da fotografia com vestes brancas. A imagem faz parte do acervo de Rosa Maria
          Antunes e é parte dos registros da Folia do Divino de 1994.
        `,
        `
          5 homens estão sentados no chão, encostados no muro baixo do curral. Eles
          aproveitam a sombra enquanto trabalham nos preparativos do Afogado. Em primeiro plano,
          do lado direito da imagem, é possível ver parte de um tacho grande onde está sendo cozido o
          Afogado. A imagem faz parte do acervo de Rosa Maria Antunes e é parte dos registros da Folia
          do Divino de 1994.
        `,
        `
          3 panelas grandes montadas sobre estruturas improvisadas aparecem do lado
          esquerdo da imagem. O cozinheiro caminha do fundo da cozinha em direção às panelas. Ao
          fundo, duas paredes bem altas de tijolos baianos sem acabamento. Um estilo típico das
          cozinhas temporárias montadas na zona rural para a preparação do Afogado nos pousos do
          Divino. A lateral direita da imagem está bastante estragada e manchada, porque ela faz parte
          de um acervo que foi molhado durante a enchente que aconteceu em São Luiz do Paraitinga
          em janeiro de 2010.
        `,
        `
          Um tacho gigantesco ocupa mais da metade da imagem. A fumaça que sai dele
          mostra que algo bem quente está fumegando. Dois homens aparecem ao lado esquerdo: um
          deles está mexendo o conteúdo do tacho com um pau de madeira e o outro está apenas
          dando um suporte. As bordas da imagem estão bastante estragadas e manchadas, porque ela
          faz parte de um acervo que foi molhado durante a enchente que aconteceu em São Luiz do
          Paraitinga em janeiro de 2010.
        `,
        `
          Um grupo de 8 pessoas está em pé, atrás de um gigantesco tacho cheio de Afogado,
          comida típica da festa do Divino de São Luiz do Paraitinga. Eles têm o semblante bem
          compenetrado e provavelmente estão rezando. O homem mais à esquerda da imagem segura
          a bandeira do Divino em um dos pousos da Folia de 1994.
        `,
        `
          A bandeira do Divino da festa de 1994 ocupa o quadro quase todo. Ela está sendo
          segurada por um senhor com um longo bigode preto. Ao seu lado, sua mulher e logo atrás seu
          filho posam para a fotografia. A imagem tem marcas de deterioração porque ela faz parte de
          um acervo que foi molhado durante a enchente que aconteceu em São Luiz do Paraitinga em
          janeiro de 2010.
        `,
        `
          A bandeira do Divino da festa de 2015 está no centro do quadro, sendo segurada
          pelo dono da casa, no momento da visita da Folia do Divino. Ao seu lado, estão o festeiro José
          de Arimatéia e o Sr. Brás da Viola, músico da folia. Atrás deles, vê-se a entrada de uma casa
          verde, bem simples, da zona rural e o chão de terra batida.
        `,
        `
          A bandeira do Divino da festa de 2015 está no centro do quadro, sendo segurada
          pela dona da casa, uma senhora bem pequena de agasalho amarelo desbotado, no momento
          da visita da Folia do Divino à sua casa. Ao seu lado, estão o festeiro José de Arimatéia e e o Sr.
          Brás da Viola, músico da folia. Atrás deles, vê-se o interior da casa, com as paredes cheias de
          quadros de santos e enfeites infantis.
        `,
        `
          A bandeira do Divino da festa de 2015 está no centro do quadro, sendo segurada
          pela dona da casa, uma senhora bem pequena de saia azul e casaco de lã, no momento da
          visita da Folia do Divino à sua casa. Ao seu lado, estão o festeiro José de Arimatéia e o Sr. Brás
          da Viola, músico da folia, além de outras 3 mulheres, provavelmente membros da mesma
          família. Atrás deles, vê-se o interior da casa, com as paredes cheias de quadros e um móvel de
          madeira, com prateleiras cheias de enfeites.
        `,
        `
          A bandeira do Divino da festa de 2015 está no centro do quadro, sendo segurada
          pelo festeiro José de Arimatéia e e o Sr. Brás da Viola, músico da folia. Atrás deles, vê-se uma
          gigantesca oca indígena feita da palha seca.
        `,
        `
          Dois senhores idosos estão sentados no pátio. Ao lado deles, uma criança de uns 4
          anos. O senhor que está sentado no meio e a criança tocam violas. Atrás deles, vê-se as
          paredes e o telhado de uma casa branca bem simples, da zona rural.
        `,
        `
          7 músicos de roupas brancas e quepe de marinheiro estão posicionados em círculo e
          fazem uma apresentação no interior de uma casa. O músico que está bem à frente da imagem
          segura uma sanfona. Mais atrás, um outro músico toda um tamborete. A bandeira do Divino
          da festa de 2015 está no centro do quadro, bem no fundo da imagem, sendo segurada por
          uma senhora que também está de roupas brancas.
        `,
        `
          Uma procissão a cavalo se aproxima pelas ruas da cidade. Os cavaleiros vêm pelo
          asfalto. Do lado direito veem-se carros estacionados. Do lado esquerdo, casas coloridas. Os
          cavaleiros chegam segurando bandeiras: o da esquerda tem nas mãos uma grande bandeira do
          Estado de São Paulo e os demais carregam bandeiras do Divino.
        `
      ]
    },
    '50b': {
      name: 'A novena antigamente',
      trail: ['Turismo'],
      alt: [
        `
          Um grupo de 10 pessoas (5 casais) caminha em fila indiana dupla durante a procissão
          da novena do Divino de 1994. É noite. A senhora que está na frente da fila segura a coroa do
          Divino. Os outros 4 casais seguram suas respectivas bandeiras. A parte superior da imagem
          está um pouco estragada e manchada, porque ela faz parte de um acervo que foi molhado
          durante a enchente que aconteceu em São Luiz do Paraitinga em janeiro de 2010.
        `,
        `
          A procissão da novena do Divino de 1994 se aproxima. Pelo piso, vê-se que eles
          estão dentro da Igreja. Casais caminham lado a lado, em fila indiana, cada um carregando a
          sua bandeira. A senhora que vai à frente da procissão carrega a coroa do Divino. As bordas da
          imagem estão bastante estragadas e manchadas, porque ela faz parte de um acervo que foi
          molhado durante a enchente que aconteceu em São Luiz do Paraitinga em janeiro de 2010.
        `,
        `
          5 pessoas enfileiradas em diagonal assistem à missa dentro da igreja matriz durante
          a novena do Divino de 1994. Eles estão de pé. Com o missário nas mãos. 3 deles seguram
          bandeiras do Divino. As bordas da imagem estão bastante estragadas e manchadas, porque ela
          faz parte de um acervo que foi molhado durante a enchente que aconteceu em São Luiz do
          Paraitinga em janeiro de 2010.
        `,
        `
          O padre, que está do lado esquerdo da imagem com roupa branco e uma grande
          túnica vermelha, abençoa a festeira (Rosa Maria Antunes) durante a missa da novena do
          Divino de 1994. Eles estão de pé. Ao fundo, veem-se outras pessoas com suas bandeiras do
          Divino. As bordas da imagem estão bastante estragadas e manchadas, porque ela faz parte de
          um acervo que foi molhado durante a enchente que aconteceu em São Luiz do Paraitinga em
          janeiro de 2010.
        `,
        `
          Um grupo de pessoas estão em pé, dentro da igreja, durante a novena do Divino de
          1994. Eles estão de pé. Todos olham para frente. Veem-se várias bandeiras do Divino: duas
          seguradas pelas pessoas que estão na fileira da frente e outras mais ao fundo da imagem. As
          bordas da imagem estão bastante estragadas e manchadas, porque ela faz parte de um acervo
          que foi molhado durante a enchente que aconteceu em São Luiz do Paraitinga em janeiro de
          2010.
        `,
        `
          O padre, que está em pé, no centro da imagem com roupa branco e uma grande
          túnica vermelha, lê um texto da Bíblia, que está apoiada em um púlpito, durante a missa da
          novena do Divino de 1994. As bordas da imagem estão um pouco estragadas e manchadas,
          porque ela faz parte de um acervo que foi molhado durante a enchente que aconteceu em São
          Luiz do Paraitinga em janeiro de 2010.
        `,
        `
          Um grupo enorme de fiéis sai da igreja em fila indiana dupla. Eles estão descendo as
          escadarias da igreja antes de iniciar uma das procissões da novena do Divino de 1994. É noite.
          A senhora que está na frente da fila segura a coroa do Divino. Veem-se várias outras bandeiras
          do Divino, nas mãos das outras pessoas que participam do evento. As bordas da imagem estão
          bastante estragadas e manchadas, porque ela faz parte de um acervo que foi molhado durante
          a enchente que aconteceu em São Luiz do Paraitinga em janeiro de 2010.
        `,
        `
          Um grupo de fiéis caminha pelas ruas da cidade durante a procissão da novena do
          Divino de 1994 segurando suas bandeiras. É noite. Ao fundo, vê-se o casario colonial da cidade,
          um edifício branco com portas e janelas azuis. As bordas da imagem estão bastante estragadas
          e manchadas, porque ela faz parte de um acervo que foi molhado durante a enchente que
          aconteceu em São Luiz do Paraitinga em janeiro de 2010.
        `,
        `
          Uma grande estrela vermelha com uma pomba branca desenhada está no centro da
          imagem. Ela é carregada por duas mulheres e passa no meio de um corredor de pessoas que
          aplaudem a sua passagem.
        `,
        `
          Um rei e uma rainha do Congo aparecem em primeiro plano. Mais atrás, o casal de
          festeiros da Festa do Divino de 1994 e alguns fiéis segurando bandeiras do Divino e, ao fundo,
          o casario colonial de São Luiz do Paraitinga. As bordas da imagem estão bastante estragadas e
          manchadas, porque ela faz parte de um acervo que foi molhado durante a enchente que
          aconteceu em São Luiz do Paraitinga em janeiro de 2010.
        `,
        `
          Nessa imagem preto e branco, do acervo de Juventinho Galhardo, uma banda com
          cerca de 20 componentes toca seus instrumentos em frente à Igreja matriz de São Luiz do
          Paraitinga. Logo atrás deles vê-se um grupo de cidadãos sentados nos bancos da praça,
          apreciando a apresentação. Mais atrás o coreto e o casario colonial da cidade.
        `
      ]
    },
    '50c': {
      name: 'O império antigamente',
      trail: ['Turismo'],
      alt: [
        `
          Altar instalado no Império do Divino. A imagem é predominantemente vermelha. A
          pomba do Divino e a Coroa da festa têm lugar de destaque nesse altar, onde se leem os
          dizeres “Sim à vida”
        `,
        `
          Altar instalado no Império do Divino. A imagem é predominantemente vermelha. A
          pomba do Divino tem lugar de destaque nesse altar que ainda é enfeitado com muitas velas
          douradas e com um tapete de flores naturais.
        `,
        `
          Altar instalado no Império do Divino. A pomba do Divino e a coroa da festa têm lugar
          de destaque nesse altar que ainda é enfeitado com muitas imagens de anjos. Mais ao fundo,
          um afresco colorido reproduz mais anjos. O Teto é dourado, feito com papel brilhante. Em
          primeiro plano, dois genuflexórios para os fiéis orarem em frente ao altar.
        `,
        `
          Vista panorâmica do império do Divino da festa de 2019. Um longo tapete vermelho
          leva até o altar. Nas paredes, ao longo do caminho, várias imagens de anjos. O teto com
          decoração predominantemente dourada e vermelha e um lustra que imita velas é o destaque
          da cena.
        `,
        `
          Fiéis carregando suas bandeiras se aglomeram para entrar na casa do império na
          festa de 2017. Veem-se apenas as cabeças das pessoas e as coroas das bandeiras que elas
          seguram e, ao fundo, a porta desse imóvel colonial, cor arcos de madeira escura que convidam
          para ver a decoração interna do império, predominantemente vermelha e dourada.
        `,
        `
          Montagem fotográfica de Antonio Carlos Lima. Da esquerda para a direita: detalhes
          do teto da sala do império, uma imagem de Nossa Senhora com as mãos em oração, close no
          altar do império onde se vê a pomba do Divino, a Coroa da festa, um globo terrestre e algumas
          velas em frente a um letreiro com os dizeres “A tua fé te salvou” e, finalmente, uma bandeira
          de um grupo de Congada, com a imagem de São Benedito, sendo segurada por uma senhora
          negra, com vestido de festa branco, vários colares no pescoço e uma coroa na cabeça.
        `,
        `
          Fiéis carregando suas bandeiras se aglomeram para entrar na casa do império na
          festa de 1994. Veem-se apenas as cabeças das pessoas e as coroas das bandeiras que elas
          seguram e, ao fundo, as janelas desse imóvel colonial e uma grande placa amarela com a
          palavra “Império” grafada em letras vermelhas.
        `,
        `
          Altar instalado no Império do Divino. A imagem é predominantemente vermelha. A
          pomba do Divino e a Coroa da festa têm lugar de destaque nesse altar, onde se leem os
          dizeres “a vida”. As laterais do altar são enfeitadas com arranjos de flores artificiais coloridas.
        `,
        `
          Altar instalado no Império do Divino de 2018. A imagem é predominantemente
          vermelha e dourada. A pomba do Divino e a Coroa da festa têm lugar de destaque nesse altar.
          A pomba, no centro do altar, está rodeada por um coração de flores vermelhas. Na parte de
          baixo desse arranjo lê-se a palavra “Caridade”. Ao lado esquerdo, um símbolo de uma âncora
          com a palavra “Esperança” e do lado direito, uma cruz com a palavra “Fé”. Há um genuflexório
          na frente do altar, mas o império está vazio nesse momento.
        `,
        `
          Fiéis carregando suas bandeiras lotam a casa do império na festa de 2017. Veem-se
          apenas as cabeças das pessoas e as coroas das bandeiras que elas seguram e, ao fundo e nas
          laterais, a decoração predominantemente vermelha e dourada do ambiente.
        `,
        `
          Vista diagonal do altar instalado no Império do Divino de 2019. A pomba do Divino e
          a coroa da festa têm lugar de destaque nesse altar que ainda é enfeitado com muitas imagens
          de anjos. Mais ao fundo, um afresco colorido reproduz mais anjos.
        `,
        `
          Detalha do teto do Império do Divino de São Luiz do Paraitinga do ano de 2017 com
          decoração predominantemente dourada e vermelha. Em primeiro plano, uma grande estrela
          azul e flores coloridas de papel crepom.
        `
      ]
    },
    '50d': {
      name: 'O afogado antigamente',
      trail: ['Turismo'],
      alt: [
        `
          Imagem preto e branco, antiga, que mostra um grupo de pessoas reunidas dentro do
          mercado municipal de São Luiz do Paraitinga. Elas estão em fila, conversando, enquanto
          esperam chegar a sua vez de receber a comida do Divino. Os homens usam ternos ou blazers e
          as mulheres vestidos e sapatos. Uma pessoa em primeiro plano espera sentada embaixo de
          um guarda-chuva preto para se proteger do sol.
        `,
        `
          Dois grandes tachos estão no chão. Eles estão cheios de comida e emanam bastante
          fumaça do calor da comida recém feita. 5 homens trabalham mexendo nos tachos para
          mistura a comida. A foto em preto e branco tem aparência bem antiga.
        `,
        `
          Close de um grande tacho de batatas, fumegando de tão quentes, montado sobre
          uma estrutura provisória de tijolos a vista com uma abertura por onde se coloca a lenha para
          esquentar a comida.
        `,
        `
          Uma multidão de pessoas aglomeradas de forma aleatória. Em primeiro plano, um
          senhor de terno, gravata e chapéu, com um prato de afogado nas mãos. Logo atrás dele, um
          homem mais novo e um grupo de senhoras também com pratos de comida nas mãos. Lá no
          fundo, vê-se o telhado da barraca onde se prepara o afogado, comida típica da festa do Divino
          de São Luiz do Paraitinga.
        `,
        `
          Um galpão enorme. Grandes espaços vazios. Da parte direita da imagem sai uma
          espécie de varal, com muitas peças enormes de carne crua penduradas. Em frente a esse varal,
          um grupo grande de voluntários trabalham cortando a carne em mesas de madeira
          improvisadas no galpão.
        `,
        `
          Um homem de camiseta branca, avental vermelho e chapéu de feltro beige pega
          pedaços de carne cortadas que estão sobre um balcão e coloca dentro de um balde de metal
          prateado. Ao fundo, vê-se uma pilha enorme de carne dentro de um tacho preto e alguns
          voluntários, de costas, cortando carne.
        `,
        `
          Dentro de uma cozinha improvisada em uma tenda, 4 mulheres, todas de avental e
          boné vermelho, carregam um enorme tacho preto de metal. O tacho está vazio. As mulheres
          estão sorrindo, divertindo-se com a atividade. Ao fundo, várias panelas menores (mas ainda
          assim bem grandes) estão lavadas e colocadas de boca para baixo para secar sobre uma mesa
          improvisada de madeira.
        `,
        `
          Com uma pá, um voluntário junta as cinzas fumegantes. Não se vê o rosto do
          homem, apenas seus sapatos sociais, suas pernas, de calça comprida e avental e suas mão
          bem rústicas e calejadas que seguram a pá. No chão, um pedaço grande de lenha parcialmente
          queimada.
        `,
        `
          Em preto e branco vê-se, em primeiro plano, o Sr. Dorvo, chefe da cozinha e
          responsável pela elaboração do Afogado de São Luiz do Paraitinga durante muitos anos. Ele
          está mexendo a comida de dentro de um tacho enorme. A fumaça que sai do caldeirão dá um
          toque mágico à cena. O Sr. Dorvo usa uma camisa social quadriculada e tem longos bigodes
          brancos. Ao fundo da cena, vários voluntários alinhados com expressão concentrada,
          provavelmente rezando.
        `,
        `
          Dois grandes caldeirões de metal preto estão alinhados. Cada um deles está apoiado
          sobre uma estrutura provisória de tijolos para que fiquem posicionados sobre a lenha que
          garante o calor para cozinhar o alimento. No centro da foto, um voluntário (Sr. Jorge Santos)
          mexe a comida com uma colher de pau gigante.
        `
      ]
    },
    '50e': {
      name: 'A cavalhada antigamente',
      trail: ['Turismo'],
      alt: [
        `
          Foto preto e branco de um grande campo gramado com a cidade de São Luiz do
          Paraitinga ao fundo. Na parte inferior direita da imagem, um grupo de 6 cavaleiros estão
          alinhados. Eles estão de frene para um outro grupo, de 8 cavaleiros, que estão do outro lado
          do campo. Uma grande plateia assiste de longe à apresentação das Cavalhadas.
        `,
        `
          Foto preto e branco de um cavaleiro segurando uma lança com a mão direita. Ele usa
          um uniforme e uma toca brancos. Está em cima do cavalo, com as rédeas na mão esquerda.
          Não se vê o cavalo. O homem tem um bigode preto bem cheio e olha em direção à câmera,
          posando para a foto.
        `,
        `
          Foto preto e branco de um cavaleiro segurando a ponta da sua espada, que está
          presa no cinto. Ele usa um uniforme e uma toca brancos. Está em cima do cavalo, com as
          rédeas na mão esquerda. Não se vê o cavalo. O homem tem um bigode preto bem cheio e olha
          em direção à câmera, posando para a foto.
        `,
        `
          Foto preto e branco de dois homens fantasiados. Eles usam panos para cobrir os
          cabelos e máscaras de papel mache. Estão abraçados e olham em direção à câmera, posando
          para a foto.
        `,
        `
          Foto preto e branco de dois homens fantasiados. Eles usam panos para cobrir os
          cabelos e máscaras de papel mache. Estão frente a frente, em posição de combate. Ao fundo,
          vários cavalos descansam na sombra de uma árvore.
        `,
        `
          Foto preto e branco de quatro homens fantasiados que seguram um quinto homem
          pelos braços e pelas pernas. Eles usam calças e camisas de mangas compridas brancas e toucas
          brancas na cabeça. Estão olhando para a câmera, posando para a foto, ao mesmo tempo que
          atuam, representando o socorro ao colega ferido pelo exército adversário. Ao fundo, vários
          cavalos pastando.
        `,
        `
          Foto em preto e branco de um cavaleiro e seu cavalo durante a apresentação das
          Cavalhadas. O homem, vestido de branco, usa uma espada para espetar uma cabeça de palha
          que está no chão, sobre a grama. O cavalo preto segue seu caminho pelo gramado sem se
          importar com os movimentos do cavaleiro. Ao fundo, vê-se a igreja matriz de São Luiz do
          Paraitinga.
        `,
        `
          Foto preto e branco de um grande campo gramado com a cidade de São Luiz do
          Paraitinga ao fundo. Em primeiro plano dois cavaleiros sobre seus cavalos posam para a foto.
          Eles estão olhando para a câmera, posando para a foto.
        `,
        `
          Foto em preto e branco de um grupo de cavaleiros executando a representação das
          Cavalhadas. Eles vêm em duplas, cada um sobre o seu cavalo e seguem em fila indiana, de
          mãos dadas com o cavaleiro da fila do lado. Todos usam os uniformes brancos típicos das
          Cavalhadas. Ao fundo da imagem vê-se a igreja matriz de São Luiz do Paraitinga.
        `,
        `
          Foto em preto e branco de um grupo de cavaleiros sobre os seus cavalos. Eles estão
          olhando para a câmera, posando para a foto. Ao fundo da imagem vê-se algumas casas São
          Luiz do Paraitinga e a torre de uma igreja.
        `
      ]
    },
    '50f': {
      name: 'As congadas antigamente',
      trail: ['Turismo'],
      alt: [
        `
          Imagem P&amp;B. Um fiel com roupa de Moçambique aparece no centro a imagem
          tocando um tambor pequeno. Atrás dele, um menino de casaco escuro e boné segura
          distraidamente um estandarte com os dizeres “1932 – Viva São Benedicto – Companhia de São
          Benedicto” e uma pintura do Santo carregando o menino Jesus. Do lado direito da imagem,
          dois brincantes batem seus pauzinhos, típicos desse tipo de dança. Do lado esquerdo, um
          senhor toca sanfona e outro toca pandeiro.
        `,
        `
          Imagem P&amp;B. Chegada de um grupo de Moçambique à cidade. À frente de grupo, 3
          meninas carregam estandartes em homenagem a São Benedito e, atrás delas, vê os músicos
          tocando seus pandeiros. Ao fundo, veem-se as casas simples da cidade e uma mata que
          provavelmente rodeia o rio Paraitinga.
        `,
        `
          Imagem P&amp;B. um grupo de Moçambique posa para a foto. Todos olhando para a
          câmera. Ao centro, um casal vestido de forma suntuosa e um rapaz negro com roupas claras
          segurando o estandarte do grupo.
        `,
        `
          Imagem P&amp;B. Em frente à igreja matriz de São Luiz do Paraitinga, um grupo de
          Moçambique posa para a foto. Duas mulheres de vestido longo branco. Dois homens de calças
          e camisas brancas e uma criança que segura um estandarte com uma pintura de São Benedito
          onde se lê “Companhia de São Benedito do Bairro da”. Atrás deles homens de terno, gravata e
          chapéu, mulheres de vestido e crianças bem arrumadas observam o movimento da praça da
          cidade.
        `,
        `
          Um grupo de Moçambique se apresenta na rua entre a praça e a igreja matriz de São
          Luiz do Paraitinga. Um homem de terno preto está de pé no centro do grupo, ao lado de uma
          senhora de vestido branco que está sentada com algum instrumento nas mãos. De cada um
          dos lados desse casal uma fileira de brincantes com roupas brancas, guizá nas pernas e
          casquete na cabeça. Algumas pessoas observam de longe, sentadas nas escadarias da igreja ou
          nas muretas da praça. Os 4 cantos dessa imagem em P&amp;B têm manchas redondas de ferrugem,
          provavelmente por terem sido presas com percevejos em algum painel.
        `,
        `
          Imagem P&amp;B. A praça central de São Luiz do Paraitinga lotada de gente. Veem -se
          especialmente muitos chapéus nas cabeças dos transeuntes. Em primeiro plano, um grupo de
          Congada tocando seus tambores enormes e cantando. Ao fundo, o casario colonial da cidade.
        `,
        `
          Um grupo de Congada tocando seus tambores enormes e cantando. Do lado
          esquerdo da imagem, um homem negro agachado toca o tambor em direção ao céu. Do lado
          direito, 4 mulheres negras com vestidos rodados, colares de contas e turbantes na cabeça
          dançam animadamente. Ao fundo, um tanto desfocado, o casario colonial da cidade.
        `,
        `
          Um homem com camisa branca, quepe de marinheiro e uma faixa verde e amarela
          cruzada no peito empunha uma espada. Não se veem seus olhos, escondidos pelo quepe.
          Atrás dele, um outro homem, com roupa similar, carrega sua espada levantada. O grupo de
          Moçambique faz uma apresentação enquanto os moradores e turistas que se veem ao fundo
          param para admirar.
        `,
        `
          Um grupo de Congada com vários homens vestido de branco com faixas verdes e
          amarelas cruzadas sobre o peito e quepes brancos aguardam na sombra enquanto seguram
          seus instrumentos de batucada. Ao fundo vê-se uma bandeira do Brasil, muitos turistas e as
          montanhas de São Luiz do Paraitinga.
        `,
        `
          Um grupo de Moçambique aguarda dentro do Império, em frente ao altar do Divino.
          3 homens carregam faixas amarelas cruzadas no peito e um outro, de costas, tem faixas azuis.
          À esquerda da imagem, um homem carregando sua sanfona e, ao centro, um outro
          carregando o estandarte do grupo.
        `,
        `
          Um grupo de Moçambique faz sua apresentação em frente ao supermercado Cursino,
          no centro de São Luiz do Paraitinga. Os brincantes que aparecem em primeiro plano estão de
          costas e levam faixas azuis claras cruzadas no peito. Eles formam uma fila indiana, com um
          participante ao lado do outro. O grupo que está do outro lado, com os componentes de frente
          para os da fila de azul, levam fitas vermelhas atravessadas no peito. Enquanto o pessoal que
          está à esquerda da foto está tocando seus instrumentos, os da direita dançam e batem seus
          pauzinhos.
        `,
        `
          Um grupo de Moçambique forma duas filas de brincantes, com os componentes de
          cada fila alinhados lado a lado e de frente para os componentes da outra fila. Todos vestem
          roupas brancas. Metade tem faixas vermelhas cruzadas no peito, a outra metade tem faixas
          azuis. Em volta deles, uma multidão de moradores e turistas se aglomera para observar a
          brincadeira.
        `,
        `
          Um grupo de Congada faz sua apresentação em frente á igreja matriz de São Luiz do
          Paraitinga. Eles estão de costas para a igreja e de frente para o coreto. Usam calças e sapatos
          brancos e camisa verde com fitas coloridas penduradas. Todos estão de chapéu. Ao fundo,
          veem-se as bandeirolas do Divino nos postes da praça, o coreto, o casario colonial e as
          montanhas da região.
        `
      ]
    },
    '50g': {
      name: 'A procissão pentecostes antigamente',
      trail: ['Turismo'],
      alt: [
        `
          Um grupo de fiéis posa para essa foto em preto e branco. Do lado direito da imagem,
          um grupo de homens carrega um andor grande. Eles estão todos de terno e gravata. Do lado
          esquerdo da imagem, um grupo de mulheres, todas de saia e blusa carregam um andor menor.
          Ao centro, uma menina de vestido e sandálias segura um oratório. Em ambas as laterais se
          veem casas baixas e simples e, ao fundo, as montanhas de São Luiz do Paraitinga.
        `,
        `
          A procissão passa pelas ruas da cidade. Caminhando da esquerda para a direita da
          foto, um grupo de homens vem à frente da procissão carregando um andor com uma santa
          enorme. Algumas pessoas estão paradas nas calçadas, de ambos os lados da rua, vendo a
          procissão passar. Tanto os participantes como os observadores da procissão estão de calças
          compridas, blazer e gravatas: a vestimenta típica da época em que essa imagem em preto e
          branco foi registrada.
        `,
        `
          Um grupo de fiéis caminha em direção ao fotógrafo. O casal que vai à frente carrega
          uma bandeira do divino. As pessoas parecem descontraídas, olhando para os lados e
          conversando entre si. Os carros antigos estacionados próximo à calcada do lado direito da
          imagem e as roupas dos fiéis mostram que a imagem em preto e branco deve ser da década de
          1970 ou 1980.
        `,
        `
          Um grupo de fiéis caminha em direção ao fotógrafo. O casal que vai à frente carrega
          uma bandeira do divino. As pessoas parecem descontraídas, olhando para os lados e
          conversando entre si. Os carros antigos estacionados próximo à calcada do lado direito da
          imagem e as roupas dos fiéis mostram que a imagem em preto e branco deve ser da década de
          1970 ou 1980.
        `,
        `
          Um grupo de fiéis caminha em direção ao fotógrafo. O casal que vai à frente carrega
          uma bandeira do divino. As pessoas parecem descontraídas, olhando para os lados e
          conversando entre si. Os carros antigos estacionados próximo à calcada do lado direito da
          imagem e as roupas dos fiéis mostram que a imagem em preto e branco deve ser da década de
          1970 ou 1980.
        `,
        `
          A procissão passa pelas ruas da cidade, ainda de terra batida, na época em que essa
          imagem foi registrada. Duas fileiras de pessoas, uma de cada lado da rua, vêm à frente da
          procissão, o centro da rua permanece vazio e, lá no fundo, vê-se membros da igreja abrindo
          alas para o andor que vem atrás. Todos os participantes os participantes da procissão estão de
          calças compridas, blazer e gravatas.
        `,
        `
          Uma multidão caminha em procissão, passando em frente à capela das Mercês em
          São Luiz do Paraitinga. Os homens vestem camisas de mangas compridas, blazers e chapéus.
          As mulheres estão de vestido. Eles caminham em direção às montanhas que se veem ao fundo,
          afastando-se do fotógrafo. A cena é impressa em preto e branco, mas o dia estava bastante
          ensolarado.
        `,
        `
          Em primeiro plano, a banda de São Luiz do Paraitinga e os músicos com seus
          instrumentos de sopro. Todos eles de terno escuro. Ao fundo, o casario antigo da cidade com
          bandeirolas do divino enfeitando as janelas. Do lado esquerdo da imagem, alguns moradores
          da cidade e turistas observam a banda enquanto aguardam a procissão de pentecostes
          começar.
        `,
        `
          Um grupo de congada carrega o mastro do Divino. Eles estão todos de calças
          brancas, camisas vermelhas e quepe branco de marinheiro. Ao fundo, barraquinhas de venda
          de lembrancinhas e turistas distraídos.
        `,
        `
          A procissão vem subindo a rua. Em primeiro plano, do lado esquerdo da imagem,
          uma pessoa carrega uma estrela enorme com uma pintura de pomba e os dizeres “Divino
          Espírito Santo – Rogai por nós”. Em seguida na fila de devotos, algumas crianças, com roupas
          de congadas seguram um estandarte onde estão pintados São Benedito, São Luiz de Tolosa e
          Nossa Senhora Aparecida. Atrás deles, outros membros de congadas com seus respectivos
          estandartes e, ao fundo, fiéis carregando bandeiras do Divino. As bordas da imagem estão
          bastante estragadas e manchadas, porque ela faz parte de um acervo que foi molhado durante
          a enchente que aconteceu em São Luiz do Paraitinga em janeiro de 2010.
        `,
        `
          6 crianças pequenas com roupas de bufão e boina cor de vinho, meias calcas e
          camisas brancas caminham à frente da procissão segurando bandeirolas onde se podem ler os
          dons do Divino, da esquerda para a direita: Temor de Deus, Conselho, Ciência e Fortaleza. As
          demais bandeirolas estão dobradas não sendo possível ler os dizeres. Atrás desse grupo de
          crianças, o padre organiza a procissão. A cidade está toda enfeitada com bandeiras do divino
          nos postes e nas janelas das casas coloniais da praça central de São Luiz do Paraitinga.
        `,
        `
          Os festeiros de 1994, Rosa Maria Antunes e seu esposo caminham pelo centro da
          cidade em procissão. Ela tem os cabelos curtos e usa um vestido preto com bolinhas brancas.
          Ele está de calça jeans e camisa social azul e carrega uma faixa vermelha atravessada no peito.
          Atrás deles, algumas pessoas carregam bandeiras do Divino. As imagem está bastante
          estragada e manchada, porque ela faz parte de um acervo que foi molhado durante a
          enchente que aconteceu em São Luiz do Paraitinga em janeiro de 2010.
        `,
        `
          A imagem, de 1971, mostra a procissão se aproximando do fotógrafo, descendo a rua
          que sai da Igreja de Nossa Senhora do Rosário (que se vê ao fundo). 6 moças vêm à frente, de
          saia longa vermelha e camisa branca de mangas compridas. Atrás delas, alguns rapazes de
          terno e gravata carregam um andor cheio de flores
        `,
        `
          Um grupo de fiéis se organiza para o início da procissão. O casal que vai à frente
          carrega uma bandeira do Divino. As pessoas parecem descontraídas, olhando para os lados e
          conversando entre si. Algumas meninas com vestidos floridos se organizam de costas para a
          foto. Provavelmente são membros do grupo de dança da fita.
        `
      ]
    },
    25: {
      name: 'Estandartes dos grupos afro',
      trail: ['Turismo'],
      alt: [
        `
          Uma fiel, brincante de Congada, segura o estandarte do seu grupo, onde se leem os
          dizeres “Grupo de Congada Filhos de N’Zambi – 1ª Congada de São Benedito, N. Sra. Rosário e
          Santa Efigênia – São José dos Campos – 17/03/2012”. Ele é feito em um tecido claro, com São
          Benedito, nossa Senhora, Santa Efigênia e o menino Jesus desenhados em frente a uma igreja.
          Ela usa saia e blusa predominantemente amarelas e está sorrindo para a foto.
        `,
        `
          Uma fiel, brincante de Congada, segura o estandarte do seu grupo: um tecido
          vermelho, com uma pomba branca e uma estrela dourada. Ela caminha em direção à câmera
          cantando, de olhos fechados. Ao seu lado, uma menina peque de vestido branco caminha
          enquanto sorri. Ao fundo, um outro estandarte com o nome do seu grupo “Caixeiras das
          nascentes – 2009”
        `,
        `
          Uma fiel, brincante de Congada, segura o estandarte do seu grupo, onde se leem os
          dizeres “Congada de São Benedito – Cidade de Socorro”. Ele é verde escuro, de veludo e tem
          bordados uma pomba branca, a imagem de São Benedito e algumas estrelas. Ela usa saia,
          blusa e turbante brancos e está sorrindo para a foto.
        `,
        `
          Uma fiel, brincante de Congada, segura o estandarte do seu grupo, onde se leem os
          dizeres “União Folclorista - São Benedito de Belém – Taubaté”. Ele é vermelho e tem pintada a
          imagem de São Benedito segurando o menino Jesus. Ela usa saia branca, mas não é possível
          ver o seu rosto porque está escondida atrás do estandarte.
        `,
        `
          Uma fiel, brincante de Congada, segura o estandarte do seu grupo, onde se leem os
          dizeres “União Folclorista - São Benedito de Belém – Taubaté”. Ele é azul claro e tem pintada a
          imagem de Nossa Senhora Aparecida. Ela usa saia e tênis brancos, mas não é possível ver o seu
          rosto porque está escondida atrás do estandarte.
        `,
        `
          Uma fiel, brincante de Congada, segura o estandarte do seu grupo, onde se leem os
          dizeres “Congada de São Benedito – Taubaté – Fundada em 24/12/95”. Ele é feito em um
          tecido vermelho, brilhante, com uma imagem pintada de São Benedito segurando o menino
          Jesus. Ela usa um vestido pink com detalhes em renda florida azul marinho, uma coroa e luvas
          brancas e está sorrindo para a foto.
        `,
        `
          Uma fiel, brincante de Congada, segura o estandarte do seu grupo, onde se leem os
          dizeres “Congada Rosa – Atibaia - São Benedito - N. Sra. Rosário”. Ele é feito em um tecido
          rosa, com uma imagem pintada de São Benedito segurando o menino Jesus e outra de Nossa
          Senhora do Rosário, também segurando o menino Jesus. Ela usa calça comprida branca e blusa
          rosa com fitas coloridas. Atrás dela, outros componentes do grupo, usando a mesma roupa.
        `,
        `
          Uma fiel, brincante de Congada, segura o estandarte do seu grupo. Ele é azul claro e
          tem pintada a imagem de Nossa Senhora Aparecida. Ela usa um vestido laranja brilhante com
          rendas pretas na parte de cima. Olha diretamente para a câmera, mas não sorri.
        `,
        `
          Uma fiel, brincante de Congada, segura o estandarte do seu grupo, onde se leem os
          dizeres “Congada de Santa Efigênia – Jardim Santa Tereza – Mogi das Cruzes”. Ele é verde com
          detalhes prateados e tem pintada a imagem de Santa Efigênia, algumas flores e algumas
          pombas. Ela usa um vestido rodado branco com bordados prateados, mas não é possível ver o
          seu rosto porque está escondido atrás do estandarte.
        `,
        `
          Uma fiel, brincante de Congada, segura o estandarte do seu grupo, onde se leem os
          dizeres “Primeira Escola de Congo de São Benedito do Erê – Tremembé / SP – Desde 2007”. Ele
          é amarelo, com detalhes vermelhos e tem pintada a imagem de São Benedito, segurando o
          menino Jesus. Ela usa vestido amarelo e vermelho com rendas douradas, mas não é possível
          ver o seu rosto porque está escurecido pela sombra do estandarte.
        `,
        `
          Um fiel, brincante de Maracatu, segura o estandarte do seu grupo, onde se leem os
          dizeres “Maracatu – Baque do Vale - 2004”. Ele é predominantemente roxo e dourado e tem
          pintadas as imagens de alguns instrumentos musicais típicos do Maracatu. O brincante usa
          calça capri lilás e um sobretudo roxo, mas não é possível ver o seu rosto porque está
          escondido atrás do estandarte.
        `
      ]
    },
    '30b': {
      name: 'Fotos da Dança da fita',
      trail: ['Turismo'],
      alt: [
        `
          Um grupo de meninas, com vestidos super coloridos, dança ao redor de um mastro
          onde estão fazendo mosaicos com fitas amarelas e vermelhas. A foto foi feita com longo
          tempo de exposição para que se pudesse ter a sensação do movimento das meninas e, por
          isso, elas não aparecem de forma nítida.
        `,
        `
          Um grupo de meninas, com vestidos super coloridos, dança ao redor de um mastro
          onde estão fazendo mosaicos com fitas amarelas e vermelhas. A foto foi feita com longo
          tempo de exposição para que se pudesse ter a sensação do movimento das meninas e, por
          isso, a maioria delas não aparecem de forma nítida. A exceção é a menina que está em
          primeiro plano, do lado esquerdo da imagem, que está parada observando o movimento das
          demais enquanto segura uma fita de cor amarela acima de sua cabeça.
        `,
        `
          Um grupo de meninas, com vestidos super coloridos, caminha em círculo ao redor de
          um mastro. Cada uma delas segura, acima a cabeça, um bastão vermelho cheio de fitas
          coloridas que escondem seus rostos.
        `,
        `
          Um grupo de meninas, com vestidos super coloridos, corre ao redor de um mastro de
          onde pendem fitas amarelas e vermelhas. A foto foi feita com longo tempo de exposição para
          que se pudesse ter a sensação do movimento das meninas. Ao fundo, vê-se a escadaria da
          Igreja matriz de São Luiz do Paraitinga repleta de pessoas sentadas assistindo à apresentação.
          Das janelas da igreja pendem grandes tecidos vermelhos com o desenho da pomba do Divino e
          labaredas de fogo que deixam claro que a apresentação é parte das celebrações da festa do
          Divino de São Luiz do Paraitinga.
        `,
        `
          6 meninas de meias brancas, sapatos pretos e saias coloridas estão em pé, olhando
          para a direita da foto. Não se veem seus rostos, apensa suas pernas. A foto foi feita com longo
          tempo de exposição para que se pudesse ter a sensação do movimento das meninas e, por
          isso, a maioria delas não aparecem de forma nítida.
        `,
        `
          Um grupo de meninas, com vestidos super coloridos, dança ao redor de um mastro
          onde estão fazendo mosaicos com fitas amarelas e vermelhas. Todas elas usam chapéus de
          palha. Ao fundo, vê-se a professora Dona Didi Andrade e alguns pais que assistem à
          apresentação. A imagem tem algumas marcas de deterioração no canto superior direito e na
          parte de baixo - ela faz parte do acervo de Rosa Maria Antunes, que foi molhado durante a
          enchente que aconteceu em São Luiz do Paraitinga em janeiro de 2010.
        `,
        `
          A procissão de pentecostes vem caminhando pelas ruas de São Luiz do Paraitinga.
          Em primeiro plano veem-se as meninas que fazem parte do grupo da dança da fita, com seus
          vestidos coloridos, chapéus de palha na cabeça. Cada uma delas segura dois bastões, um na
          mão direita e outro na mão esquerda e cada bastão é segurado também pela menina que está
          ao seu lado, formando uma espécie de cordão de abre alas. Atrás delas vem o padre e muitos
          fiéis carregando as suas bandeiras. A imagem tem muitas marcas de deterioração porque ela
          faz parte do acervo de Rosa Maria Antunes, que foi molhado durante a enchente que
          aconteceu em São Luiz do Paraitinga em janeiro de 2010
        `,
        `
          Uma multidão aglomerada de forma organizada, em círculo, todos olhando para o
          centro da roda, onde está o mastro da dança da fita. Em volta dele, no primeiro círculo, um
          grupo de meninas com seus vestidos coloridos, uniforme do grupo durante as apresentações.`,
          `Um grupo de meninas, com vestidos floridos, dança ao redor de um mastro onde
          estão fazendo mosaicos com fitas. A foto é preto e branco então não é possível saber as cores
          das fitas. No centro da roda, segurando o mastro, um menino de bermuda social e camisa
          preta olha para cima para ver o desenho que está sendo formado a partir da performance das
          meninas.
        `,
        `
          A procissão de pentecostes vem caminhando pelas ruas de São Luiz do Paraitinga.
          Em primeiro plano veem-se as meninas que fazem parte do grupo da dança da fita, com seus
          vestidos floridos e chapéus de palha na cabeça. Cada uma delas segura dois bastões, um na
          mão direita e outro na mão esquerda e cada bastão é segurado também pela menina que está
          ao seu lado, formando uma espécie de cordão de abre alas. Atrás delas vêm muitos fiéis
          carregando as suas bandeiras e, mais ao fundo, vê-se o casario colonial de São Luiz do
          Paraitinga e as montanhas. A foto bastante antiga é preto e branco e está bastante bem
          conservada.
        `
      ]
    },
    '30a': {
      name: 'Fotos do Teatro',
      trail: ['Turismo'],
      alt: [
        `
          Um homem com o rosto pintado de preto, com grandes olhos pintados de branco, a
          boca e as orelhas vermelhas. Ele faz cara de surpresa e decepção. Está agachado, atrás de uma
          banqueta, espiando o movimento. Usa uma camisa branca de mangas compridas e um avental
          preto todo sujo de tinta.
        `,
        `
          Dois palhaços: o da frente sentado, segurando uma sanfona fechada, com os braços
          cruzados em volta dela e cara de bravo. O de trás, em pé, segura uma espécie de lança,
          totalmente recoberta com fitas coloridas. Está de olhos fechados e boca aberta, como se
          estivesse gritando algo. Os dois têm os rostos pintados de branco com detalhes exagerados
          pintados para ampliar a sensação de expressividade.
        `,
        `
          Um palhaço vestido de anjo. O rosto pintado de branco, com lábios bem vermelhos e
          sobrancelha gigante bem preta. Sobre a cabeça uma coroa de gravetos amarelos encimados
          por uma pomba branca de feltro com fitinhas coloridas. Ele olha para o lado esquerdo da
          imagem, fazendo um biquinho, enquanto leva as mãos juntas na frente do peito em posição de
          prece.
        `,
        `
          Uma palhaça com cara de brva, o rosto pintado de branco, as bochechas bem
          vermelhas e uma boquinha bem pequena em formato de coração. Ela está em pé, segurando
          placas de tecido quadriculadas de verde e vermelho. Ao fundo vê-se, de forma desfocada,
          parte da plateia que assiste à apresentação.
        `,
        `
          3 palhaços fazem pose interagindo entre si. Do lado esquerdo um homem alto e
          magro com uma sanfona pendurada no peito e os braços nos quadris. Ele olha para as duas
          moças com cara de quem está dando uma bronca. As duas moças, uma de peruca azul e outra
          de peruca vermelha de tule estão de costas uma para a outra. Não se vê o rosto da moça de
          peruca vermelha. A de peruca azul, com o rosto todo maquiado de forma bem exagerada olha
          para o público como quem pede aprovação. Ao fundo vê-se, de forma desfocada, parte da
          plateia que assiste à apresentação.
        `,
        `
          Uma palhaça fantasiada de bruxa olha para a direita da imagem com os olhos
          arregalados e a boca aberta. Ela usa um vestido de tule preto e uma peruca de tule super
          exagerada, também preta, com umas fitinhas vermelhas formando laços que se espalham pela
          cabeleira.
        `,
        `
          Uma palhaça de roupa vermelha com bolinhas pretas olha para o público e sorri. Ela
          tem o rosto todo pintado de branco, as bochechas bem vermelhas, um coração desenhado
          sobre os lábios e as sobrancelhas exageradamente pretas. Usa óculos escuros e um chapéu de
          fuxicos com dois grandes chifres vermelhos, um virado para cima e outro virado para baixo
        `,
        `
          Um palhaço de calça azul clara, camisa branca e um babador gigantesco bordado
          com lantejoulas faz pose sobre uma baqueta. Ele tem um apito na boca, um que branco de
          marinheiro enfeitado com fuxicos vermelhos e segura uma espécie de lança recoberta de
          fitinhas coloridas. Ao fundo vê-se, de forma desfocada, parte da plateia que assiste à
          apresentação.
        `,
        `
          Um homem de turbante branco e longas barbas pretas encara alguém com
          expressão severa.
        `,
        `
          Uma multidão se reúne para assistir à apresentação de teatro. As pessoas estão
          sentadas em um grande círculo, espalhadas pelos degraus da escadaria da igreja matriz de São
          Luiz do Paraitinga e pelo chão na rua e na praça. No centro do círculo, veem-se, ao longe, os
          atores e suas quinquilharia coloridas, realizando a sua performance.
        `
      ]
    },
    '30e': {
      name: 'Fotos da Fanfarra',
      trail: ['Turismo'],
      alt: [
        `
          No centro da imagem vê-se o estandarte da Banda Monsenhor Ignacio Gioia (BAMIG)
          de São Luiz do Paraitinga. Segurando o estandarte, estão duas meninas de farda azul marinho
          com detalhes dourados, botas brancas de cano alto e quepes de marinheiro. Atrás delas,
          outras crianças enfileiradas carregam as bandeiras do Brasil e do estado de São Paulo apoiadas
          nos ombros.
        `,
        `
          Cerca de 20 músicos se aproximam, caminhando em direção à fotógrafa. Eles vêm
          em 4 fileiras, cada um com seu instrumento de sopro na boca. Usam um longo sobretudo nas
          cores vermelho, dourado e azul marinho, calças e sapatos brancos e quepes de marinheiro na
          cabeça.
        `,
        `
          Uma menina de farda azul marinho com detalhes dourados e vermelhos e quepe de
          marinheira caminha da direita para a esquerda da imagem carregando a bandeira do Brasil
          com o mastro apoiado no seu ombro direito. Ela olha fixamente para frente, concentrada em
          sua importante tarefa. Ao fundo vê-se, de forma desfocada, parte da plateia que assiste à
          apresentação.
        `,
        `
          Uma homem de farda azul marinho com detalhes dourados e vermelhos e quepe de
          marinheiro caminha da direita para a esquerda da imagem tocando um instrumento de sopro.
          Ele olha fixamente para frente, concentrado em sua importante tarefa.
        `,
        `
          Uma multidão se reúne para assistir à apresentação da fanfarra. As pessoas estão
          sentadas em um grande círculo, espalhadas pelos degraus da escadaria da igreja matriz de São
          Luiz do Paraitinga e pelo chão na rua e na praça. No centro do círculo, veem-se, ao longe,
          algumas meninas com bandeiras amarelas e azuis, realizando a sua performance. Sobre a
          multidão, uma chuva de fitinhas vermelhas e brancas fazendo as vezes de teto de um local
          aberto.
        `,
        `
          5 meninas com bandeiras amarelas e azuis, farda azul marinho com detalhes
          vermelhos e dourados e botas brancas de cano alto realizam a sua performance. Ao fundo, de
          forma desfocada, vê-se uma multidão que assiste à apresentação. Sobre eles, uma chuva de
          fitinhas vermelhas e brancas fazendo as vezes de teto de um local aberto.
        `,
        `
          5 meninas com bandeiras amarelas e azuis, farda azul marinho com detalhes
          vermelhos e dourados e botas brancas de cano alto realizam a sua performance. A foto, feita
          com longo tempo de exposição, dá destaque para o movimento das bandeiras, que parecem
          grandes círculos translúcidos de cor amarela. Ao fundo, de forma desfocada, vê-se uma
          multidão que assiste à apresentação. Sobre eles, uma chuva de fitinhas vermelhas e brancas
          fazendo as vezes de teto de um local aberto.
        `,
        `
          Uma menina de farda azul marinho com detalhes dourados e vermelhos segura sua
          bandeira que está apoiada no chão. Ela tem uma postura imponente olha fixamente para
          frente, concentrada em sua importante tarefa. Ao fundo vê-se, de forma desfocada, parte da
          plateia que assiste à apresentação.
        `,
        `
          Os membros da fanfarra se em um grande círculo, cada um tocando seu
          instrumento. Todos eles com o mesmo uniforme: um sobretudo azul com detalhes vermelhos
          e dourado, calças brancas e quepe de marinheiro.
        `,
        `
          Uma menina com vestido azul bordado faz uma apresentação de dança na calçada
          em frente à Igreja matriz de São Luiz do Paraitinga. Ela está parada, olhando para a câmera,
          fazendo uma pose. Ao fundo, vê-se uma multidão que ocupa todos os degraus da escadaria da
          igreja para assistir à sua apresentação.
        `
      ]
    },
    '30f': {
      name: 'Fotos do Pau de sebo',
      trail: ['Turismo'],
      alt: [
        `
          Do lado esquerdo da imagem, na diagonal, sai um mastro de madeira e várias
          bandeirolas brancas e vermelhas que enfeitam o céu completamente azul. Abraçado ao
          mastro, vê-se um homem de bermuda cinza chumbo, descalço e sem camisa, que pisa sobre os
          ombros de um outro homem enquanto olha para baixo.
        `,
        `
          Do lado direito da imagem, na diagonal, sai um mastro de madeira e várias
          bandeirolas brancas e vermelhas que enfeitam o céu completamente azul. Abraçado ao
          mastro, vê-se um homem de calça jeans, camiseta azul e boné vermelho, que ajoelha sobre os
          ombros de um outro homem de bermuda cinza e camiseta bege, que, por sua vez, está em pé
          sobre os ombros de um homem sem camisa que está de costas.
        `,
        `
          Um homem de bermuda bege e camiseta cinza está se segurando em um tronco de
          madeira com a mão direita, enquanto, com a esquerda, faz sinal para que alguém venha ao
          seu encontro. Ao fundo veem-se bandeirolas brancas que cortam o céu completamente azul
        `,
        `
          As ruas de São Luiz do Paraitinga, com seu casario colonial colorido são o cenário
          para uma imagem de uma multidão aglomerada no canto esquerdo que olha toda numa
          mesma direção: um mastro de madeira com cerca de 8 meninos empilhados, cada um pisando
          nos ombros do anterior, tentando escalar o mastro e alcançar algo que está preso lá em cima.
        `,
        `
          No centro da imagem, um mastro de madeira. Abraçado ao mastro, vê-se, de costas,
          um homem de bermuda florida e camiseta preta, que está em pé sobre os ombros de dois
          outros homens sem camisa que estão de costas. O céu completamente azul ao fundo.
        `,
        `
          No centro da imagem, um mastro de madeira. Abraçado ao mastro, vê-se, de costas,
          um homem de bermuda branca e sem camisa, que está em pé sobre os ombros de um outro
          homem sem camisa que abraça o mastro com toda sua força. O céu completamente azul ao
          fundo e a torre da igreja matriz de São Luiz do Paraitinga.
        `
      ]
    },
    '30g': {
      name: 'Fotos dos Bonecões',
      trail: ['Turismo'],
      alt: [
        `
          Bonecões João Paulino e Maria Angu posam para as câmeras em frente à Igreja
          matriz de São Luiz do Paraitinga. Ao fundo, num nicho da parede da igreja, a imagem de São
          Luiz de Tolosa e, mais acima, uma bandeira do Divino com a pomba branca, uma labareda de
          fogo e a palavra “fortaleza”, um dos dons do Divino.
        `,
        `
          O bonecão de Maria Angu atravessa a praça central de São Luiz do Paraitinga. É
          possível ver a mão da pessoa que lhe dá vida no buraco do vestido preto com flores brancas e
          vermelhas. Uma criança lhe dá a mão e faz pose de glória. Ao fundo, a igreja matriz e as
          bandeirolas que enfeitam a cidade durante a festa do Divino.
        `,
        `
          O bonecão de João Paulino atravessa a praça central de São Luiz do Paraitinga. Ao
          fundo, a igreja matriz e as bandeirolas que enfeitam a cidade durante a festa do Divino
        `,
        ` 
          Bonecões João Paulino e Maria Angu e um grupo de 11 motoqueiros que vieram para
          a festa posam para as câmeras em frente à Igreja matriz de São Luiz do Paraitinga.
        `,
        `
          Bonecão João Paulino e um grupo enorme de crianças posam para as câmeras em
          frente à Igreja matriz de São Luiz do Paraitinga. Pelas roupas das crianças a foto deve ser da
          década de 1970 ou 1980.
        `,
        `
          Bonecão João Paulino caminha pelas ruas de São Luiz do Paraitinga e diverte um
          grupo enorme de crianças que o provocam e depois fogem dele. Pelas roupas das crianças a
          foto deve ser da década de 1970 ou 1980.
        `,
        `
          Os bonecões Maria Angu e João Paulino (mais ao fundo) caminham pelas ruas de São
          Luiz do Paraitinga e divertem um grupo enorme de crianças que os provocam e depois fogem
          deles. Pelas roupas das crianças a foto deve ser da década de 1970 ou 1980.
        `,
        `
          Os bonecões Maria Angu e João Paulino caminham pelas ruas de São Luiz do
          Paraitinga e divertem um grupo enorme de crianças que os provocam e depois fogem deles.
          Pelas roupas das crianças a foto deve ser da década de 1970 ou 1980.
        `,
        `
          Os bonecões João Paulino e Maria Angu caminham pelas ruas de São Luiz do
          Paraitinga, descendo a Rua Monsenhor Ignacio Gioia e divertem um grupo pequeno de
          crianças que acompanham a uma certa distância. Pelas roupas das crianças a foto deve ser da
          década de 1970 ou 1980.
        `,
        `
          Os bonecões João Paulino e Maria Angu caminham pelas ruas de São Luiz do
          Paraitinga e divertem um grupo grande de crianças que acompanham a uma certa distância.
          Pelas roupas das crianças a foto, em preto e branco, deve ser da década de 1960.
        `,
        `
          Ao longe e de costas, vê-se os bonecões João Paulino e Maria Angu caminhando
          pelas ruas de São Luiz do Paraitinga, seguidos de perto por um grupo enorme crianças. Pelas
          roupas das crianças a foto, em preto e branco, deve ser da década de 1960.
        `
      ]
    }
  }
}

export default gallerys
