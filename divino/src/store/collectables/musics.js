const musics = {
  name: 'Músicas',
  url: 'Musicas',
  description: `
    <p>Conforme vai navegando pelo site, você vai ganhando acesso às
    músicas que compõem a trilha sonora da festa.</p>
    <p>As músicas vão sendo desbloqueadas à medida que você tiver assistido aos
    vídeos dos locais onde cada uma delas foi tocada nesse webdocumentário.</p>
  `,
  color: '#40068B',
  items: {
    '32c': {
      name: 'Brincadeiras dos Pousos',
      artist: 'Amarildo Pereira Marcos',
      musics: [
        'Calango',
        'Cana Verde',
        'Congada 1',
        'Congada 2',
        'Danca do caranguejo',
        'Danca do sabão',
        'Desafio',
        'Jongo',
        'Lundum',
        'Maracatu',
        'Mocambique',
        'Sao Goncalo'
      ]
    },
    '32d': {
      name: 'Celebrando o Divino',
      artist: 'Pedro Moradei',
      musics: [
        'Pequei, Senhor',
        'Sequência',
        'A ti meus dons',
        'Diga Sim',
        'Cordeiro',
        'Veni Creator Spiritus',
        'Nossa Bandeira'
      ]
    },
    '32a': {
      name: 'A nós descei divina luz',
      artist: '',
      musics: [
        { title: 'A nós descei divina luz', artist: 'Jacque Falcheti' },
        { title: 'A nós descei divina luz', artist: 'Padre Álvaro Mantovani' }
      ]
    },
    '32f': {
      name: 'Morada Campesina',
      artist: 'Rui Kleiner',
      musics: [
        { title: 'Catireteira', artist: 'Rui Kleiner e Paulinho Leme' },
        { title: 'Dandara - Lundu', artist: 'Rui Kleiner' },
        { title: 'Morada Campesina', artist: 'Rui Kleiner' }
      ]
    },
    '32e': {
      name: 'Eternos dobrados',
      artist: 'Corporação Musical São Luiz de Tolosa',
      musics: [
        { title: 'Sargento Caveira', artist: 'Antonio M Espírito Santo' },
        { title: 'Coronel Manuel Rabelo', artist: 'João Nascimento' },
        { title: 'Newton Alves Ferreira', artist: 'Luiz Antonio Ivo Salinas' },
        { title: 'Rubens Moraes Sarmento', artist: 'Pedro Salgado' },
        { title: '4 dias de viagem', artist: 'João Nascimento' },
        { title: 'Brasil', artist: 'T. Cardoso' }
      ]
    },
    '32b': {
      name: 'Divina Benção',
      artist: 'Galvão Frade',
      musics: [
        { title: 'Festa do Divino', artist: 'Afonso Pinto' },
        { title: 'Não tem Jeito', artist: 'Galvão Frade' },
        { title: 'Dança do Sabão', artist: 'Domínio público' },
        { title: 'Dança do Caranguejo', artist: 'Domínio público' }
      ]
    }
  }
}

export default musics
