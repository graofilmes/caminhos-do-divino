const postcards = {
  name: 'Postais',
  url: 'Postais',
  description: `
  <p>Conforme vai navegando pelo site, você vai ganhando cartões
  postais: são recordações de cada um desses lugares que você pode enviar para
  seus amigos com uma mensagem personalizada.</p>
  <p>Os postais vão sendo desbloqueados à medida que você tiver assistido aos
  vídeos dos locais aos quais cada um deles se refere.</p>
  `,
  color: '#D30D2B',
  items: {
    '33p': {
      name: 'Afogado no Pouso',
      alt: `
        Dois tachos grandes de metal estão sobre o fogo, apoiados em estruturas temporárias
        de tijolos. Ao fundo, uma freira benze a comida e vários fieis observam
        compenetrados. O fogo faz os tachos ficarem acobreados e, apesar de serem de
        alumínio, parecem de um metal nobre. O chão de terra batida e a cobertura de telhas
        aparentes é parte do avarandado da casa do fiel que dará o pouso à bandeira durante
        a Folia do Divino.
      `
    },
    '33q_1': {
      name: 'Foliões do Divino',
      alt: `
        Os foliões do Divino estão em pé, formando um pequeno círculo, bem no centro a
        imagem. Eles sorriem e batem palmas. Um pouco mais atrás, um senhor segura a
        bandeira do festeiro, cheia de fitas e de fotografias. Em volta deles, várias pessoas se
        aglomeram para participar do momento de fé e diversão.
      `
    },
    '33q_2': {
      name: 'Viola',
      alt: `
        O braço de uma viola, com os prendedores das 10 cordas aparece em primeiro plano
        no lado direito da imagem. Ao fundo, bem sem foco, vislumbram-se algumas pessoas
        sentadas.
      `
    },
    '33o_1': {
      name: 'Dança de São Gonçalo',
      alt: `
        Em primeiro plano, uma capelinha pintada de azul e branco vista de costas. A cruz
        que está em cima dela se destaca pela presença de uma fita vermelha. Ao fundo,
        desfocado, vê-se um violeiro tocando sua viola em uma evento em homenagem a São
        Gonçalo.
      `
    },
    '33o_2': {
      name: 'Jongo Caboclo',
      alt: `
        Em frente a uma grande fogueira, dois homens tocam os tambores sobre os quais
        estão sentados (instrumentos musicais e banquinhos, ao mesmo tempo). Um deles
        olha para o lado direito, para fora da foto, como se estivesse conferindo o andamento
        do Jongo que está rolando e para o qual eles estão tocando.
        `
    },
    '33d_1': {
      name: 'Missa do Divino',
      alt: `
        A nave da Igreja repleta de fiéis. Eles estão de costas, olhando para o altar. Vários
        deles seguram suas bandeiras do Divino. No meio dos dois blocos de fieis, o corredor
        da igreja está vazio, deixando à mostra o azulejo estampado típico desse monumento
        histórico.
      `
    },
    '33d_2': {
      name: 'Festa do Divino',
      alt: `
        As torres da igreja de São Luiz do Paraitinga são vistas ao longe, um pouco mais
        adiante, a copa de uma árvore carregada de flores cor de rosa e, ainda mais adiante,
        fileiras de bandeirolas brancas e cor de rosa que foram instaladas para enfeitar a
        cidade. O céu completamente azul deixa claro o dai lindo que estava fazendo.
      `
    },
    '33e_2': {
      name: 'Novena',
      alt: `
        Uma coroa de bandeiras do Divino, com flores de tecido vermelhas, uma escultura de
        pomba branca de asas abertas. Ao fundo, veem-se os enfeites da rua cruzando o céu
        preto da noite.
      `
    },
    '33e_1a': {
      name: 'Banda',
      alt: `
        Ocupando toda a rua de uma calçada até a outra, várias fileiras de músicos da banda
        São Luís de Tolosa posam para uma fotografia. Eles estão todos de terno preto e
        camisa branca e cada um deles segura o seu instrumento musical. Nas laterais das
        imagens pode-se ver o casario colonial colorido da Rua do Carvalho, no centro
        histórico de São Luiz do Paraitinga.
      `
    },
    '33e_1b': {
      name: 'Música na procissão',
      alt: `
        Uma partitura musical ocupa o centro da imagem. Olhando para ela, de costas para a
        foto e visto de forma apenas parcial, um músico de terno preto toca um instrumento
        de sopro. Ao fundo, um pouco desfocado, veem-se outros músicos com seus
        instrumentos e suas partituras participando da procissão de pentecostes em São Luiz
        do Paraitinga.
      `
    },
    '33s': {
      name: 'Os 7 Dons do Divino',
      alt: `
        7 fitas de cetim estão alinhadas lado a lado sobre um tecido vermelho. Cada uma
        delas tem uma cor e um texto diferente de identificação. Da esquerda para a direita: a
        fita amarela tem a palavra “ciência”, a fita azul marinho tem a palavra “piedade”, a
        fita verde tem a palavra “conselho”, a fita roxa tem as palavras “temor de Deus”, a
        fita azul clara tem a palavra “Sabedoria”, a fita vermelha tem a palavra “fortaleza” e a
        fita branca tem a palavra “entendimento”. As 7 cores que representam os 7 dons do
        Divino Espírito Santo.
      `
    },
    '33a_1': {
      name: 'Divino coroado',
      alt: `
        Uma coroa enfeitada com pequenas flores coloridas de tecido envolve uma pequena
        escultura de uma pomba branca de asas abertas. De baixo da estrutura onde estão as
        flores e a pomba estão amarradas fitas de cetim coloridas que pendem do arranjo. É a
        Coroa típica das bandeiras do Divino. Ao fundo, bastante desfocado pode-se ver o
        casario colonial de São Luiz do Paraitinga.
      `
    },
    '33a_2': {
      name: 'Decoração da cidade',
      alt: `
        Um casarão colonial com 6 grandes janelas é visto de frente nessa imagem. As janelas
        têm um madeiramento amarelo que fecha o vão impedindo que se veja dentro da
        casa, e persianas brancas que estão abertas. Em cada uma delas há ainda uma cortina
        branca rendada (que cobre a metade superior do vão) e uma escultura de madeira
        com uma pomba branca rodeada de 7 flores coloridas (representando as cores dos 7
        dons do Divino) apoiada na parte de baixo do batente, de onde também estão
        pendentes tecidos dourados cobertos por rendas vermelhas. Uma prova do charme
        da decoração das casas de São Luiz do Paraitinga durante os dias da festa do Divino.
      `
    },
    '33a_3': {
      name: 'Pomba do Divino',
      alt: `
        Uma pequena escultura de madeira, no formato de uma pomba branca, de asas
        abertas, repousa sobre um globo terrestre e ocupa o centro da imagem. Em volta da
        escultura, uma imagem bastante desfocada, veem-se bolinhas amarelas e vermelhas.
      `
    },
    '33b_1': {
      name: 'Bandeiras na missa',
      alt: `
        Duas coroas de bandeiras do Divino, com suas flores de tecido coloridas e suas fitas
        pendentes estão em primeiro plano, na parte esquerda da imagem. Ao fundo, veem-
        se os fiéis dentro da igreja e uma escultura de Jesus crucificado.
      `
    },
    '33b_2': {
      name: 'O Filho e o Espírito Santo',
      alt: `
        Uma coroa de bandeiras do Divino, com flores de tecido vermelhas, uma pomba
        branca de asas abertas e várias fitas coloridas pendentes estão em primeiro plano, na
        parte esquerda da imagem. Ao fundo, vê-se uma escultura de Jesus crucificado.
      `
    },
    '33b_3': {
      name: 'Bandeiras no Império',
      alt: `
        Várias bandeiras do Divino com seus tecidos vermelhos e suas coroas de flores
        artificiais estão encostadas na parede, dentro do Império que está todo decorado
        com papel brilhante dourado e veludo vermelho.
      `
    },
    '33c': {
      name: 'Fitas e Fotos',
      alt: `
        Uma coroa de bandeiras do Divino, com flores de tecido vermelhas, uma pomba
        branca de asas abertas, várias fitas coloridas pendentes e duas fotografias 3 por 4
        presas às fitas estão em primeiro plano, na parte esquerda da imagem. Ao fundo,
        veem-se os fiéis dentro da igreja e uma escultura de uma santa.
      `
    },
    '33r': {
      name: 'Império',
      alt: `
        Vista diagonal do altar instalado no Império do Divino de 2019. A pomba do Divino e a
        coroa da festa têm lugar de destaque nesse altar que ainda é enfeitado com muitas
      `
    },
    '33i': {
      name: 'Preparação do Afogado',
      alt: `
        Uma nuvem de fumaça branca esconde um homem de quem não se pode ver o rosto.
        Ele está distraído, olhando para o lado esquerdo, de camisa polo e boné vermelhos.
      `
    },
    '33j': {
      name: 'Comida Sagrada',
      alt: `
        Em primeiro plano, um caldeirão gigantesco cheio de carne e batatas picadas. Mais
        atrás, um grupo de cerca de 25 pessoas, quase todas de roupas vermelhas e touca de
        proteção na cabeça rezam junto com o padre que está abençoando a comida na
        cerimonia que antecede a distribuição do Afogado, comida típica da festa do Divino
        de São Luiz do Paraitinga
      `
    },
    '33k': {
      name: 'Voluntários do Afogado',
      alt: `
        Dois grandes caldeirões de metal preto estão alinhados. Cada um deles está apoiado
        sobre uma estrutura provisória de tijolos para que fiquem posicionados sobre a lenha
        que garante o calor para cozinhar o alimento. No centro da foto, um voluntário (Sr.
        Jorge Santos) mexe a comida com uma colher de pau gigante.
      `
    },
    '33l_1': {
      name: 'Cristãos e Mouros',
      alt: `
        Sobre um gramado bem verdinho, quatro cavaleiros montados em seus cavalos fazem
        uma apresentação teatral. Eles vêm do fundo do campo em pares: os cavaleiros do
        lado direito de camisa e quepe azul, representando os cristãos; os do lado esquerdo
        de camisa e quepe vermelho, representando os mouros. Cada um deles segura uma
        espada. Cada cristão tem a sua espada cruzada com a de um mouro, simulando um
        momento de batalha. Ao fundo vê-se a plateia, acomodada no gramado, assistindo à
        apresentação.
      `
    },
    '33l_2': {
      name: 'Cavaleiros habilidosos',
      alt: `
        Um cavalo branco corre pelo gramado conduzido por um homem fantasiado de
        cavaleiro mouro. Ele usa calça jeans, camisa e gorro vermelhos e segura uma lança na
        mão direita, em posição de ataque. A imagem foi feita seguindo o movimento do
        cavalo e, por isso, o fundo está todo borrado e não se pode ver direito a plateia que
        estava sentada do lado oposto do gramado.
      `
    },
    '33l_3': {
      name: 'Devoção dos Cavaleiros',
      alt: `
        6 cavaleiros montados sobre seus cavalos e com lanças nas mãos estão parados de
        forma alinhada em frente à Igreja de São Luiz do Paraitinga. Não se veem os seus
        rostos, porque estão de costas para a foto, mas sabe-se que representam os mouros,
        pela cor de suas roupas. Acima de suas cabeças, bandeirolas brancas e cor de rosa
        enfeitam a rua e fazem contraste com o céu completamente azul de um dia super
      `
    },
    '33m_1': {
      name: 'Alvorada',
      alt: `
        No alvorecer do domingo de Pentecostes, a bruma ainda não se dissipou totalmente e
        a igreja matriz de São Luiz do Paraitinga quase se funde com o céu, totalmente
        branco. Caminhando pelas ruas vemos um grupo de Congada que chega à cidade para
        a festa. Eles estão de costas para a fotógrafa e carregam seus bumbos e o estandarte
        do grupo. Os músicos estão de camisetas azuis. A mulheres, com vestidos de festa
        longos, rodados e coloridos, típicos das apresentações de Congadas.
      `
    },
    '33m_2': {
      name: 'Congada na praça',
      alt: `
        Um grupo de Congada com roupas de chita colorida e aventais brancos dança na
        sombra das árvores na rua lateral da praça da matriz de São Luiz do Paraitinga. Ao
        fundo vê-se o casario colonial da cidade, todo ensolarado e, em cada janela uma
        bandeirola vermelha pendurada, identificando a data como a Festa do Divino.
      `
    },
    '33m_3': {
      name: 'Reverência',
      alt: `
        Um homem negro está de costas para a câmera segurando a aba de seu chapéu. A
        imagem é bem de close e o chapéu é o maior destaque: ele está inteiro bordado de
        lantejoulas coloridas (uma faixa verde em cima, seguida de uma vermelha, uma
        branca e uma azul) no centro do chapéu uma imagem de Santa Efigênia segurando
        uma casa e um rosário branco de plástico adornam a indumentária típica das
        Congadas que vêm a São Luiz do Paraitinga no dia de Pentecostes para participar da
        festa do Divino.
      `
    },
    '33m_4': {
      name: 'Moçambique',
      alt: `
        Dois homens de costas. Ambos estão com roupas brancas enfeitadas com duas faixas
        vermelhas atravessadas no peito e fitinhas coloridas de Nossa Senhora Aparecida
        pendendo da roupa. Só se veem os detalhes desta indumentária típica dos
        Moçambiques que vêm a São Luiz do Paraitinga no dia de Pentecostes para participar
        da festa do Divino.
      `
    },
    '33f': {
      name: 'Postal da Dança da Fita',
      alt: `
        Um grupo de meninas, com vestidos super coloridos, dança ao redor de um mastro
        onde estão fazendo mosaicos com fitas amarelas e vermelhas. A foto foi feita com
        longo tempo de exposição para que se pudesse ter a sensação do movimento das
        meninas e, por isso, elas não aparecem de forma nítida.
      `
    },
    '33g': {
      name: 'Postal do Teatro de rua',
      alt: `
        Uma palhaça fantasiada de bruxa olha para a direita da imagem com os olhos
        arregalados e a boca aberta. Ela usa um vestido de tule preto e uma peruca de tule
        super exagerada, também preta, com umas fitinhas vermelhas formando laços que se
        espalham pela cabeleira.
      `
    },
    '33h_1': {
      name: 'Postal da Fanfarra',
      alt: `
        5 meninas com bandeiras amarelas e azuis, farda azul marinho com detalhes
        vermelhos e dourados e botas brancas de cano alto realizam a sua performance. A
        foto, feita com longo tempo de exposição, dá destaque para o movimento das
        bandeiras, que parecem grandes círculos translúcidos de cor amarela. Ao fundo, de
        forma desfocada, vê-se uma multidão que assiste à apresentação. Sobre eles, uma
        chuva de fitinhas vermelhas e brancas fazendo as vezes de teto de um local aberto.
      `
    },
    '33h_2': {
      name: 'Postal do Pau de Sebo',
      alt: `
        No centro da imagem, um mastro de madeira. Abraçado ao mastro, vê-se, de costas,
        um homem de bermuda branca e sem camisa, que está em pé sobre os ombros de
        um outro homem sem camisa que abraça o mastro com toda sua força. O céu
        completamente azul ao fundo e a torre da igreja matriz de São Luiz do Paraitinga.
      `
    },
    '33h_3': {
      name: 'Postal dos Bonecões João Paulino e Maria Angu',
      alt: `
        Close nos rostos dos bonecões João Paulino e Maria Angu. Ele de camisa branca,
        colete azul, tem um bigode preto bem grosso e usa um chapéu de palha. Ela tem o
        cabelo escuro, preso atrás das orelhas e usa um vestido florido
      `
    },
    '33n': {
      name: 'Procissão de Pentecostes',
      alt: `
        Vista de longe e de baixo, a igreja matriz de São Luiz do Paraitinga ocupa quase a
        totalidade dessa imagem. A foto foi tirada no justo momento em que o andor do
        Divino chegava de volta à igreja, no final da procissão de Pentecostes e uma chuva de
        papel prateado picado caia das janelas do 2º andar da igreja. Lá em baixo, uma
        multidão de fieis se reúne, cobrindo completamente a escadaria da igreja.
      `
    }
  }
}

export default postcards
