const documents = {
  name: 'Curiosidades',
  url: 'Extras',
  description: `
  <p>Conforme vai navegando pelo site, você vai ganhando acesso a
  informações adicionais e curiosidades sobre especificidades e histórias
  relacionadas à festa.</p>
  <p>Os documentos vão sendo desbloqueados à medida que você tiver acessado
  os mesmos conteúdos ao longo da navegação e foram colocados aqui apenas
  para facilitar sua localização futura.</p>
  `,
  color: '#008240',
  items: {
    '08': {
      name: 'Origem do Afogado',
      type: 'text',
      trail: ['Conhecimento'],
      doc: `
        <div class="reis">
        <figure>
        <img alt="pintura representando Don Diniz" src="` + require('@/assets/afogado/af1.jpg') + `" />
        <figcaption>D. Dinis, 6° rei de Portugal</figcaption>
        </figure>
        <figure>
        <img alt="pintura representando Don Afonso" src="` + require('@/assets/afogado/af2.jpg') + `" />
        <figcaption>D. Afonso IV, 7° rei de Portugal</figcaption>
        </figure>
        </div>
        <h1>Uma família em pé de guerra</h1>
        <p><span class="first-letter">O</span> ano era 1320. O rei D. Dinis e seu filho legítimo e herdeiro do trono, D. Afonso, estavam brigados.  A rainha, D. Isabel de Aragão, estava arrasada.</p>
        <p>Ela só tinha tido 2 filhos: D. Afonso e D. Constança (que a essas alturas já tinha se casado, mudado para a Espanha, tido 3 filhos, enviuvado e morrido). </p>
        <p>Bom, sendo assim, em 1320, pouco antes de completar 50 anos, a rainha Isabel tinha um único filho vivo, D. Afonso, que andava brigando com o pai por não se conformar que o Rei D. Dinis, tinha mais afinidades com seu filho bastardo D. Afonso Sancho.</p>
        <p>Não era só uma briguinha, claro, era algo bastante sério. O infante andava usando todas as artimanhas que lhe vinham à mente para dissuadir o pai da ideia de nomear o irmão bastardo como seu sucessor ao trono de Portugal e usava para isso alguns métodos beeeem questionáveis, por exemplo, inventando mentiras para difamar o meio irmão e tentando (mais de uma vez) matá-lo.</p>
        <p><span class="first-letter">M</span>esmo que a rainha, D. Isabel de Aragão, não compactuasse com os métodos do seu filho, afinal ela era muito pacífica, ela também não se conformava que fosse desejo do rei que a coroa portuguesa passasse, após sua morte, para seu filho bastardo.</p>
        <p>Diante do conflito aparentemente insolúvel, a rainha passou a suplicar ao Divino pela paz entre seu esposo e seu filho.</p>
        <p>A rainha era muito devota e tinha muita fé no Divino Espírito Santo para quem organizava, todos os anos desde 1296, uma missa, seguida de um bom almoço, para o qual convidava o clero, a nobreza e o povo, no dia de Pentecostes. </p>
        <p>Em 1323 a situação de conflito entre pai e filho era tão grave que D. Dinis juntou suas tropas para se defender de um exército que estava sendo montado pelo filho D. Afonso. Ele mandou um emissário para o forte onde o filho e seu exército estavam reunidos e, para sua surpresa, o emissário foi ameaçado pelo próprio príncipe e só não foi morto por intervenção de outros fidalgos presentes. O príncipe, irritadíssimo, juntou o seu exército e se pôs a caminho do forte onde estavam o rei e seus aliados. As tropas do rei também se aproximaram. De ambos os lados, as lanças da cavalaria e os montantes, rijamente empunhados, esperavam apenas o sinal para entrar em ação. Realmente não era só uma briga boba...</p>
        <h1>Santa Isabel e o Divino Espírito Santo entram em ação</h1>
        <img alt="pintura representando santa isabel" src="` + require('@/assets/afogado/af4.jpg') + `" />
        <p><span class="first-letter">T</span>odos os homens posicionados no campo de batalha e eis que surge, subitamente, a figura veneranda e majestosa da rainha D. Isabel, montada em uma pequena mula. </p>
        <p>Ninguém se atreveu a avançar, ninguém pensou sequer em impedir a passagem da rainha. </p>
        <p>Conta-se que “todos se sentiam profundamente emocionados e de muitos olhos corriam lágrimas”. Mãe dolorosa, esposa desolada, ídolo de plebeus e de nobres, a sua passagem erguia uma barreira luminosa entre as duas hostes. A Rainha Santa Isabel, tão querida de todos, apoiada pela sua fé no Espírito Santo de Deus, acudira a tempo ao esposo amargurado e ao filho ambicioso e a batalha de Alvalade não chegou a acontecer.</p>
        <p>Ufa!</p>
        <p>A rainha então, acreditando que foi abençoada pela intervenção do Divino Espírito Santo, prometeu intensificar a caridade, especialmente por meio da distribuição de esmolas e comida e, mesmo contrariando muitas vezes o rei, manteve a sua promessa e suas atividades filantrópicas.</p>
        <p>Conta a lenda (conhecida como "o milagre das rosas") que o rei, já irritado por ela andar sempre misturada com mendigos, a proibiu de dar mais esmolas. Mas, certo dia, vendo-a sair furtivamente do castelo, foi atrás dela e perguntou o que ela levava escondido por baixo do manto.</p>
        <p>Era pão. Mas ela, aflita por ter desobedecido ao rei, exclamou:</p>
        <p>- São rosas, Senhor!</p>
        <p>- Rosas, em janeiro? - duvidou ele.</p>
        <p>De olhos baixos, a rainha Santa Isabel abriu o cesto e os pães tinham-se transformado em rosas, tão lindas como jamais se viu.</p>
        <p>Mais uma vez, o Divino protegeu a santa rainha por estar agindo bem e pagando a sua promessa. </p>
        <p>D. Diniz morreu em 1325, pouco depois de ter firmado a paz com o príncipe herdeiro. Estavam ao seu lado no momento da sua morte, a Rainha, o herdeiro D. Afonso, dois bastardos e o restante da corte.</p>
        <h1>A herança de Santa Isabel com um toque de brasilidade</h1>
        <p>Em honra à rainha e à sua promessa de doar comidas aos pobres é que surgiu a tradição da distribuição gratuita de alimentos no dia de Pentecostes.</p>
        <p>No Vale do Paraíba, no interior de São Paulo, o pão foi substituído pelo Afogado: um ensopado de carne de vaca, servido com farinha de mandioca, típico da comida caipira com receita originária do Vale do Paraíba e um dos alimentos preferidos dos tropeiros na saga da busca do ouro nas Minas Gerais.</p>
        <p>Desta maneira, une-se a história dos nossos colonizadores, aos costumes locais da população cabocla, mas permanece a essência divina da fraternidade e da igualdade entre todos, usualmente associada, na religião católica, ao Espírito Santo de Deus.</p>
        <p></p>
        <img alt="pintura representando Santa Isabel" src="` + require('@/assets/afogado/af3.jpg') + `" />
      `
    },
    '09': {
      name: 'Receita do Afogado',
      type: 'Receitas',
      trail: ['Turismo']
    },
    12: {
      name: 'As cores dos 7 dons',
      type: 'Cores',
      trail: ['Fe', 'Conhecimento']
    },
    14: {
      name: 'Como montar a sua bandeira',
      type: 'Slides',
      trail: ['Turismo'],
      slides: [
        {
          cover: true,
          img: '0'
        },
        {
          title: 'Defina o tamanho da sua bandeira',
          img: '1',
          content: '<p>Muita gente quer uma bandeira grande, porque, além de mais bonita, ela é uma homenagem mais vistosa. Só que, antes de definir o tamanho da sua bandeira, você deve pensar no peso que o conjunto todo vai ter e, consequentemente, o peso que você vai ter que carregar todos os dias da novena.</p>'
        },
        {
          title: 'Escolha a Coroa',
          img: '2',
          content: '<p>Além de pensar no estilo da coroa e da pomba que irão coroar a bandeira, é importante pensar que seu design pode ajudar a tornar mais fácil (ou mais difícil) a amarração das fitas e de objetos que você e outros devotos queiram dedicar ao Divino.</p>'
        },
        {
          title: 'Escolha o tecido da bandeira',
          img: '3',
          content: '<p>A bandeira sempre tem a cor vermelha, que simboliza o fogo, alusivo à forma pela qual o Espírito Santo se manifestou aos apóstolos. Só que tecidos vermelhos costumam desbotar com facilidade e acabam ficando desbotados e sem brilho: escolha um tecido de boa qualidade se quiser usá-lo por vários anos.</p>'
        },
        {
          title: 'Prenda as fitas à Coroa',
          img: '4',
          content: `<p>Você sabia que aquelas fitas coloridas além de deixarem a bandeira mais alegre e bonita têm um significado?</p>
              <p>São 7 no total, cada uma delas com uma cor diferente, que representa um dos dons do Divino:</p>
              <ol><li>Amarelo: ciência</li>
                <li>Vermelho: fortaleza</li>
                <li>Azul claro: sabedoria</li>
                <li>Azul escuro: piedade</li>
                <li>Verde: conselho</li>
                <li>Roxo: temor de Deus</li>
                <li>Prata: entendimento</li></ol>`
        },
        {
          title: 'Benção das bandeiras novas',
          img: '5',
          content: '<p>As bandeiras que participam pela primeira vez de uma novena são todas abençoadas em um mesmo dia: o primeiro sábado do Pentecostes. Nesse dia, elas têm que ser levadas à Igreja, para receber a benção especial do padre. A partir dessa data, ela passa a ser de fato um símbolo do Divino.</p>'
        },
        {
          title: 'Adornando a sua bandeira',
          img: '6A',
          content: `<p>Cada coisa que as pessoas penduram na sua bandeira tem um significado: pode ser um agradecimento, um pedido ou uma promessa, não há regras, mas representa sempre algo relevante para a pessoa que a colocou ali.</p>
              <p>Você pode pendurar coisas em qualquer bandeira, não apenas na sua. A bandeira do festeiro, por exemplo, costuma ficar cheia de adereços já que é a que mais circula durante o ano. Ao final do período de festas, chega a pesar mais de 7kg de tantos adornos que recebe.</p>`
        },
        {
          title: 'Adornando a sua bandeira',
          img: '6B',
          content: `<p>Os nozinhos representam os pedidos dos fiéis. Cada nozinho é um pedido.</p>
              <p>Para escolher onde dar os nós, às vezes as pessoas medem seu próprio corpo ou o de alguém da família. Por exemplo, se tenho um problema no pulso que quero pedir ajuda ao Divino para solucionar, enrolo a fita no pulso para medir o seu tamanho e dou um nó naquela altura da fita.</p>
              <p>Muita gente, depois de dar o nó, corta a parte de baixo da fita e leva consigo, como um lembrete desse pedido. Por isso as fitas nem sempre têm o mesmo tamanho em bandeiras que já participaram da festa.</p>`
        },
        {
          title: 'Saudando a bandeira',
          img: '7',
          content: '<p>É comum as pessoas beijarem a bandeira ou fazerem o sinal da cruz cada vez que cruzam com alguma pelas ruas. Como a bandeira é o símbolo do Divino, ao saudá-la, você está saudando Deus ali representado.</p>'
        },
        {
          title: 'Guardando a bandeira fora do período da festa',
          img: '8',
          content: `<p>A Bandeira tem que ficar em um lugar de destaque na sua casa o ano todo, representando a presença do Divino.</p>
              <p>O tecido, muitas vezes, é retirado para não estragar e desbotar, mas a coroa e o mastro devem ficar o ano todo expostos.</p>`
        },
        {
          title: 'O descarte ou a reciclagem da bandeira',
          img: '9',
          content: `<p>Quando a bandeira (ou alguma parte dela) vai ser reciclada ou refeita, o material retirado tem que ser queimado ou enterrado, porque é um material abençoado que não pode ser simplesmente jogado no lixo.</p>
              <p>Pode parecer estranho alguém se desfazer de uma bandeira, mas é comum, por exemplo, que o tecido velho fique puído e precise ser substituído por um novo depois de alguns anos de uso.</p>`
        }
      ]
    },
    39: {
      name: 'Versos de São Gonçalo',
      sub: 'Amarildo Pereira Marcos Lagoinha - SP',
      type: 'text',
      trail: ['Conhecimento'],
      doc: `
        <ul>
          <li>São Gonçalo do Amarante, Guerreiro de Portugal oi ai<br />
          Que nóis precisa nóis pede, o que nóis pede ele vale oi oi ai.</li>
          <li>Meu glorioso São Gonçalo, é o São Gonçalo do Amarante, <br />
          São Gonçalo é o mais velho, toca sua viola adiante oi oi ai.</li>
          <li>Meu glorioso São Gonçalo, é um Santo tão perfeito oi ai<br />
          Vamos dançar a sua dança, debaixo de muito respeito oi, oi ai.</li>
          <li>Dança sério meus irmãos, e não vamos acassoá oi ai<br />
          São Gonçalo é milagroso, também pode castiga oi, oi ai.</li>
          <li>Dança sério meus irmãos, aqui não é dança de sala oi ai<br />
          Nóis pode ser castigado do Glorioso São Gonçalo oi, oi ai.</li>
          <li>Uma Excelência, do Glorioso São Gonçalo oi ai<br />
          Que nóis precisa nóis pede, o que nóis pede ele vale oi, oi ai.</li>
          <li>Duas Excelência, do Glorioso São Gonçalo oi ai<br />
          Que nóis precisa nóis pede, o que nóis pede ele vale oi, oi ai.</li>
          <li>Três Excelência, do Glorioso São Gonçalo oi ai<br />
          Que nóis precisa nóis pede, o que nóis pede ele vale oi, oi ai.</li>
          <li>Quatro Excelência, do Glorioso São Gonçalo oi ai<br />
          Que nóis precisa nóis pede, o que nóis pede ele vale oi, oi ai.</li>
          <li>Cinco Excelência, do Glorioso São Gonçalo oi ai<br />
          Que nóis precisa nóis pede, o que nóis pede ele vale oi, oi ai.</li>
          <li>Seis Excelência, do Glorioso São Gonçalo oi ai<br />
          Que nóis precisa nóis pede, o que nóis pede ele vale oi, oi ai.</li>
          <li>Sete Excelência, do Glorioso São Gonçalo oi ai<br />
          Que nóis precisa nóis pede, o que nóis pede ele vale oi, oi ai.</li>
          <li>Oito Excelência, do Glorioso São Gonçalo oi ai<br />
          Que nóis precisa nóis pede, o que nóis pede ele vale oi, oi ai.</li>
          <li>Nove Excelência, do Glorioso São Gonçalo oi ai<br />
          Que nóis precisa nóis pede, o que nóis pede ele vale oi, oi ai.</li>
          <li>Dez Excelência, do Glorioso São Gonçalo oi ai<br />
          Que nóis precisa nóis pede, o que nóis pede ele vale oi, oi ai.</li>
          <li>Onze Excelência, do Glorioso São Gonçalo oi ai<br />
          Que nóis precisa nóis pede, o que nóis pede ele vale oi, oi ai. </li>
          <li>Doze Excelência, do Glorioso São Gonçalo oi ai<br />
          Que nóis precisa nóis pede, o que nóis pede ele vale oi, oi ai.</li>
          <li>São Gonçalo do Amarante, toca sua violinha oi ai<br />
          Casamenteiro das moças, e também das viuvinhas oi, oi ai.</li>
          <li>São Gonçalo do Amarante, Santo de toda Virtude oi ai<br />
          Conservai o seu devoto com muita vida e saúde oi, oi ai.</li>
          <li>São Gonçalo do amarante, Guerreiro de Portugal oi ai<br />
          Ajudai a nóis vence esta batalha do mal oi, oi ai.</li>
          <li>Pai Filho e Espírito Santo, do Divino Sacramento oi ai<br />
          Dá licença São Gonçalo, pra nóis fazer o beijamento oi, oi ai.</li>
          <li>Chega pecador contrito, aos pés de Deus pedi perdão oi ai<br />
          Do Glorioso São Gonçalo Receber Santa Bênção oi, oi ai.</li>
          <li>São Gonçalo foi pro Céu, como num raio de luz oi ai<br />
          Foi levar a nossa dança, na presença de Jesus oi, oi ai.</li>
          <li>São Gonçalo está no Céu, Junto ao pai da imensidão oi ai<br />
          Na mesa do seu devoto não deixa faltar o pão oi, oi ai.</li>
          <li>Meu Glorioso São Gonçalo, a sua dança vai parar oi ai <br />
          Se acaso houve algum erro, pra nóis queira perdoá oi, oi ai.</li>
        </ul>
      `
    },
    41: {
      name: 'A origem das Cavalhadas',
      type: 'Slides',
      trail: ['Conhecimento'],
      slides: [
        {
          cover: true,
          img: 1
        },
        {
          title: 'A Cavalhada no Brasil',
          content: `
            <p>A Cavalhada veio para o Brasil seguindo uma tradição de
            origem portuguesa da época medieval, quando os aristocratas
            exibiam a sua destreza e valentia nos torneios medievais.</p>
            <p>Esses torneios, frequentemente, envolviam encenações de
            temas da chamada Reconquista - período que vai de 718 (com
            a chegada dos mouros) a 1492 (com a conquista do Reino de
            Granada pelos reinos cristãos).</p>
            <p>A representação teatral que deu origem às Cavalhadas se
            baseia em um poema épico da literatura francesa, escrito por
            volta do ano 1100, chamado “A Canção de Rolando” que é uma
            das cerca de 20 canções que constituem a chamada “Gesta do
            Rei”.</p>
            <p>Em São Luiz do Paraitinga, assim como em outras cidades
            brasileiras, um grupo de 24 cavaleiros reencena, todos os anos,
            a guerra entre os mouros (vestidos de vermelho) e os cristãos
            (vestidos de azul), que termina sempre com a conversão dos
            mouros ao cristianismo.</p>
          `
        },
        {
          title: 'As guerras da Reconquista',
          content: `
            <img alt="Imagem de homem cavalgando com paramentos da Cavalhada"
            src="` + require('@/assets/slides/41/2.png') + `" />
            <p>Em 710 d. C., a Península Ibérica era um território dominado
            pelos visigodos, povo cuja monarquia era eletiva.</p>
            <p>Com a morte do rei Vitiza, as cortes se reuniram para eleger o
            seu sucessor. Havia dois candidatos: o grupo de Ágila II (filho
            de Vitiza) e o grupo de Rodrigo (rei visigodo de Toledo).</p>
            <p>Os partidários de Ágila II solicitaram apoio ao vice-rei do
            Norte da África (o muçulmano Muça Ibne Noçair) e o
            convidaram para enviar uma expedição militar à Península.</p>
            <p>Muça enviou o general Tárique, cujos exércitos
            desembarcaram em Gibraltar dia 30 de abril de 711, de onde
            deram início a uma série de lutas em apoio a Ágila II.</p>
            <p>Em setembro de 711, derrotaram Rodrigo, que faleceu, na
            batalha de Guadalete.</p>
            <p>Nos anos seguintes, os muçulmanos foram assenhoreando-se
            do território designado em língua árabe como Al-Andalus e
            abrindo as portas para que a população oriunda do Norte da
            África, cruzasse o estreito de Gibraltar e penetrassem na
            península Ibérica.</p>
            <p>O que ficou posteriormente conhecida como a ”invasão
            muçulmana” começou, na verdade, como uma ocupação
            territorial pelos soldados muçulmanos que ali chegaram e que
            acabaram governando a região por quase oitocentos anos.</p>
            <p>Houve resistência em várias partes da Península, mas a
            desunião ibérica favoreceu bastante os muçulmanos. Os
            reinos ibéricos eram monarquias feudais, o que era eficiente
            para combater incursões muçulmanas, mas dificultava o
            processo de Reconquista devido à desunião dinástica e às
            guerras feudais.</p>
            <p>A ideia de guerra santa, pela cruz cristã, só veio a surgir na
            época das Cruzadas (1096) quando foram escritas as canções
            de gesta, inclusive a Canção de Rolando e a reconquista de
            todo o território peninsular, só foi concluída em 1491 com a
            tomada do reino muçulmano de Granada pelos reis católicos.</p>
          `
        },
        {
          title: 'A Canção de Rolando',
          content: `
            <img alt="Imagem do livro A Canção de Rolando, edição Martins Fontes"
            src="` + require('@/assets/slides/41/livro.jpg') + `" />
            <p>A Canção de Rolando, a canção de gesta na qual se baseia a
            Cavalhada, tem como pano de fundo um evento histórico: a
            morte de Rolando, sobrinho de Carlos Magno e um dos 12
            Pares de França, em um combate que aconteceu em
            Roncesvales, nos Pirineus, em 15 de agosto de 778.</p>
            <p>O poema é composto de 4002 versos agrupados em 290
            estrofes. As estrofes têm um número irregular de versos
            decassílabos, sem rimas, mas com o emprego da assonância
            (repetição de vogais com sons semelhantes nas sílabas
            tônicas).</p>
            <p>A batalha de Roncesvalles, na verdade, foi um combate contra
            os bascos e não contra os mouros, mas numa época em que a
            Igreja Católica temia a ameaça muçulmana (que dominava a
            Península Ibérica a Ocidente e a Terra Santa a Oriente) e em
            que era necessário recrutar pessoas para participar das
            Cruzadas, era natural que as canções de gesta tratassem
            majoritariamente do tema da guerra santa contra os
            muçulmanos e, por isso, o autor tomou a liberdade de alterar
            os fatos e substituir os bascos pelos mouros em sua história.</p>
            <p>Além disso, apesar dos acontecimentos reais terem sido pouco
            gloriosos, a lenda fez de Rolando um mártir cristão contra o
            islamismo.</p>
            <p>Na lenda, ele é considerado um modelo de virtude da cavalaria
            e se distinguiu pela sua valentia, salvando o exército de seu tio
            do desastre.</p>
          `
        },
        {
          title: 'As canções de gesta ou Gestas do Rei',
          content: `
            <p>As canções de gesta (em francês chansons de geste) são um
            conjunto de poemas épicos da literatura francesa.</p>
            <p>Elas foram pensadas para ser recitadas por cantores
            profissionais - jograis e menestréis - nas vilas, nas cidades e
            nas cortes dos nobres da época.</p>
            <p>Os temas de quase todas essas canções estão ligados à época
            do rei francês Carlos Magno (que viveu de 742 a 814) e a
            maioria delas é baseada em lendas ou na imaginação do autor
            que usa muitos paralelismos e repetições, contando e
            recontando algumas cenas de ângulos um pouco diferentes.</p>
            <p>Em geral a narrativa é rápida, mas algumas passagens são
            contadas em muitos detalhes.</p>
            <p>Quase não há descrição psicológica dos personagens, que são
            caracterizados mais por suas ações do que por suas reflexões.</p>
          `
        },
        {
          title: 'Sinopse da Canção de Rolando',
          content: `
          <img alt="Imagem de homem cavalgando com paramentos da Cavalhada"
          src="` + require('@/assets/slides/41/3.png') + `" />
          <p>A Canção de Rolando fala dos 7 anos que Carlos Magno
          passou na Espanha (no século VII) lutando contra os mouros e
          de quando Marsílio, o rei de Saragoça (única cidade que
          Carlos Magno ainda não tinha conquistado!) faz uma proposta
          de rendição.</p>
          <p>Certo de que a derrota era inevitável, Marsílio desenvolveu
          um plano para ludibriar os francos: prometeu que seria
          vassalo de Carlos Magno e que se converteria ao cristianismo,
          uma vez que o imperador tivesse partido da Espanha. Ele não
          pensava em concretizar o prometido, era tudo parte de um
          grande plano para fazer com que os francos saíssem do seu
          território.</p>
          <p>Rolando não confia na palavra de Marsílio, mas Ganelão (seu
          padrasto) lembrou que os vassalos de Carlos Magno estavam
          cansados da guerra e defendeu a ideia de chegar a um acordo
          para promover a paz.</p>
          <p>Os conselheiros do imperador decidiram então enviar uma
          embaixada a Saragoça. Era uma empreitada perigosa porque
          Marsílio já havia matado enviados anteriores. Então Rolando
          sugeriu Ganelão como embaixador e o rei concordou. Isso
          aumentou o ódio que Ganelão sentia contra Rolando.</p>
          <p>Ganelão viajou a Saragoça e, num tenso encontro com
          Marsílio, os dois armaram um plano para matar Rolando e
          seus companheiros. Ganelão convenceu os sarracenos que,
          com o sobrinho morto, Carlos Magno perderia o ânimo para
          lutar. Garantiu que convenceria o imperador a colocar
          Rolando no comando da retaguarda do exército, o que tornaria
          mais fácil para o exército inimigo acabar com a vida do conde.</p>
          <p>Ganelão convence Carlos Magno das boas intenções de
          Marsílio e conseguiu que Rolando se convertesse no
          comandante da retaguarda.</p>
          <p>O conde, um dos doze pares de França, ia acompanhado por
          vinte mil homens. Quando chegaram no passo de
          Roncesvalles, a retaguarda foi vítima de uma emboscada,
          atacados por vários batalhões de sarracenos, no total, 400
          milhares de homens, os francos lutam valentemente. Rolando
          se recusa a soar o olifante - uma trombeta - para avisar as
          tropas de Carlos Magno e receber ajuda. Rolando, com sua
          espada Durindana e seu cavalo Vigilante, é o que mais
          inimigos derrota, mas os sarracenos são muitos e não há
          esperança para os cristãos.</p>
          <p>Quando já não há mais que sessenta francos vivos, Rolando,
          usando as suas últimas forças, finalmente toca o olifante, para
          que Carlos Magno possa vir vingá-los. Antes que a ajuda
          finalmente chegasse, morrem todos os francos desse batalhão.
          A alma de Rolando é levada ao céu por anjos e santos.</p>
          <p>Carlos Magno e os seus homens, ao chegar, chocam-se com a
          visão do massacre.</p>
          <p>Ocorre então um milagre: o sol deixa de girar no céu,
          impedindo que anoiteça, e assim os sarracenos não podem se
          esconder na escuridão. O exército franco persegue os infiéis
          até ao rio Ebro e os oponentes que não morrem pela espada,
          acabam por morrer afogados no rio.</p>
          <p>Marsílio retorna a Saragoça, onde o ânimo dos muçulmanos é
          fraco. A sua mão direita foi decepada durante a batalha. Um
          poderoso emir da Babilônia, chamado Baligante vem socorrer
          o seu vassalo. E num combate entre Baligante e Carlos Magno,
          o rei franco recebe a ajuda divina e derrota o emir. O exército
          franco então aproveita a oportunidade e toma Saragoça,
          destruindo todos os itens religiosos islâmicos e judaicos da
          cidade. Todos os habitantes da cidade são obrigados a
          converter-se ao catolicismo exceto a rainha Bramimonda, que
          é levada ao país dos francos, para que aceite espontaneamente
          o cristianismo.</p>
          <p>Em Aquisgrão, a capital dos francos, começa o julgamento de
          Ganelão. Pinabel, eloquente parente de Ganelão, convence os
          jurados de que o réu traiu Rolando, mas não o seu senhor,
          Carlos Magno. Thierry, um corajoso, mas débil cavaleiro,
          argumenta que trair Rolando foi o mesmo que trair o rei, e
          desafia Pinabel a um combate. Durante a luta, com
          intervenção divina, Thierry vence. Ganelão é executado
          cruelmente: cada um dos seus braços e pernas são atados a um
          cavalo, que puxam cada um numa direção e assim o seu corpo
          é esquartejado. Outros parentes de Ganelão, que tinham
          ficado do seu lado na disputa, são também executados.</p>
          <p>Bramimonda aceita o cristianismo e é batizada.</p>
          <p>Tudo parece estar finalmente em paz, mas, durante a noite, o
          anjo Gabriel aparece para Carlos Magno num sonho
          anunciando que ele deve partir para mais uma guerra contra
          os pagãos. Cansado, mas obediente, Carlos Magno prepara-se
          para mais batalhas na guerra santa contra os infiéis.</p>
        `
        }
      ]
    },
    42: {
      name: 'A Origem do Império',
      type: 'text',
      trail: ['Conhecimento'],
      doc: `
        <p><span class="first-letter">C</span>onta-se que a festa do Divino aconteceu pela primeira vez em 1296, em Alenquer, Portugal, quando a rainha D. Isabel convocou o clero, a nobreza e o povo para celebrarem juntos o dia de Pentecostes. Nesse dia, inauguraram uma confraria a que chamaram de Império. </p>
        <p>A ideia de convidar o povo para as celebrações estava associada à prova de humildade dos reis e à ideia de que, para Deus, todos são iguais.</p>
        <p>Desse modo, de todos as pessoas do povo que participaram dos ofícios religiosos, realizados na capela real, escolheu-se o mais pobre para ocupar o dossel da capela-mor: o lugar do rei.</p>
        <p>O rei, por sua vez, lhe serviu de cortesão e seus mais altos dignitários foram os pajens. </p>
        <p><span class="first-letter">D</span>urante a cerimônia, esse homem se ajoelhou sobre o almofadão destinado ao rei e foi coroado pelo bispo com a coroa real, enquanto, entoava o hino "Veni Creator Spiritus". Ele assistiu à celebração da missa assim, investido de todas as insígnias reais e, em seguida, se dirigiu ao paço real, onde lhe foi oferecido um farto jantar, servido pela rainha.</p>
        <p>Esta solenidade encontrou eco nos fidalgos da corte, que desejavam seguir o exemplo de humildade dos seus soberanos e assim, com a autorização do monarca, mandaram fazer coroas semelhantes à do rei (tendo ao centro um medalhão com os símbolos da Santíssima Trindade) e passaram a fazer cerimônias idênticas à realizada no paço real, todos os anos, no dia de Pentecostes. </p>
        <p>Foi daí então que surgiu a tradição de usar o mesmo cerimonial da corte de D. Dinis e D. Isabel na festa do Divino.</p>
        <p><span class="first-letter">O</span> Império do Divino, parte desse cerimonial, é um local previamente construído para acolher os principais símbolos da festa: a imagem do Espírito Santo, as bandeiras do festeiro e as dos devotos, a coroa, o cetro e a salva (bandeja onde se depositam esmolas). É também o local onde se pode encontrar o imperador (presente até hoje em muitas dessas festas é, em alguns lugares, representado por uma criança): aquele que foi escolhido para portar a coroa e o cetro reais durante as procissões. </p>
        <p>Em São Luiz do Paraitinga, a coroa, confeccionada em prata, data de 1873 - ano em que provavelmente foi doada à comunidade (porque a festa existe há mais tempo do que isso).</p>
      `
    },
    46: {
      name: 'Lista de festeiros',
      sub: 'Festa do Divino de São Luiz do Paraitinga',
      type: 'text',
      trail: ['Conhecimento'],
      doc: `
      <table>
        <tr><td>Aguinaldo Salinas</td><td class="date">1942</td></tr>
        <tr><td>Benedito Pião Sobrinho</td><td class="date">1943</td></tr>
        <tr><td>Ernesto José de Toledo</td><td class="date">1944</td></tr>
        <tr><td>Benedito Lopes</td><td class="date">1945</td></tr>
        <tr><td>Mons. José Maria G. Mariano <br />e Congregação</td><td class="date">1946</td></tr>
        <tr><td>Benedito Hermínio da Silva</td><td class="date">1947</td></tr>
        <tr><td>José da Silva Pião</td><td class="date">1948</td></tr>
        <tr><td>Bento Vaz de Campos</td><td class="date">1949</td></tr>
        <tr><td>Mons. José Maria G. Alves <br />e Comissão</td><td class="date">1950</td></tr>
        <tr><td>Wilson de Campos Coelho</td><td class="date">1951</td></tr>
        <tr><td>Sebastião Coelho</td><td class="date">1952</td></tr>
        <tr><td>João Batista Cardoso</td><td class="date">1953</td></tr>
        <tr><td>Benedito de Campos</td><td class="date">1954</td></tr>
        <tr><td>Fernando Maia</td><td class="date">1955</td></tr>
        <tr><td>José de Oliveira Barros</td><td class="date">1956</td></tr>
        <tr><td>Igreja e Comissão</td><td class="date">1957</td></tr>
        <tr><td>Daniel Pereira Coelho</td><td class="date">1958</td></tr>
        <tr><td>José Teófilo Galhardo Filho</td><td class="date">1959</td></tr>
        <tr><td>Benedito Antunes Figueira</td><td class="date">1960</td></tr>
        <tr><td>Francisco Rodrigues da Silva</td><td class="date">1961</td></tr>
        <tr><td>Geraldo Rodrigues Monteiro</td><td class="date">1962</td></tr>
        <tr><td>Oswaldo Farias Soares</td><td class="date">1963</td></tr>
        <tr><td>Conego Antonio Luiz Cursino Santos</td><td class="date">1964</td></tr>
        <tr><td>Santa Casa de Misericórdia</td><td class="date">1965</td></tr>
        <tr><td>Benedito deo Espírito Santo Campos</td><td class="date">1966</td></tr>
        <tr><td>Otavio Pereira de Campos</td><td class="date">1967</td></tr>
        <tr><td>Manoel Lopes Pereira</td><td class="date">1968</td></tr>
        <tr><td>Mons. Tarcisio de Castro Moura</td><td class="date">1969</td></tr>
        <tr><td>José André Ribeiro</td><td class="date">1970</td></tr>
        <tr><td>Idalicio Maia</td><td class="date">1971</td></tr>
        <tr><td>Mons. Tarcisio de Castro Moura</td><td class="date">1972</td></tr>
        <tr><td>João Batista Maia</td><td class="date">1973</td></tr>
        <tr><td>Geraldo Rodrigues Carvalho</td><td class="date">1974</td></tr>
        <tr><td>Silverio Rangel Sobrinho</td><td class="date">1975</td></tr>
        <tr><td>José da Silva Pião</td><td class="date">1976</td></tr>
        <tr><td>Grupo São Patableo</td><td class="date">1977</td></tr>
        <tr><td>Grupo São Patableo</td><td class="date">1978</td></tr>
        <tr><td>Joaquim José Maia</td><td class="date">1979</td></tr>
        <tr><td>Alfredo dos Santos</td><td class="date">1980</td></tr>
        <tr><td>Sebastião Pereira Bonfim</td><td class="date">1981</td></tr>
        <tr><td>Igreja, Comissão <br />e Simpósio Internacional</td><td class="date">1982</td></tr>
        <tr><td>Vicente Lourenço da Silva</td><td class="date">1983</td></tr>
        <tr><td>Anesio Rodrigues da Silva</td><td class="date">1984</td></tr>
        <tr><td>João Eugênio de Patablea</td><td class="date">1985</td></tr>
        <tr><td>Luiz Mariano Rodrigues</td><td class="date">1986</td></tr>
        <tr><td>Ari Rangel dos Santos</td><td class="date">1987</td></tr>
        <tr><td>Benedito Pires Santos</td><td class="date">1988</td></tr>
        <tr><td>Luiz da Silva Mendonça</td><td class="date">1989</td></tr>
        <tr><td>Sebastião Lobo dos Santos</td><td class="date">1990</td></tr>
        <tr><td>Comissão de Festeiros</td><td class="date">1991</td></tr>
        <tr><td>José Eugenio Filho, <br />José Benedito dos Santos <br />e Nestor Antonio Santana</td><td class="date">1992</td></tr>
        <tr><td>Jorge da Silva Pião, <br />Geraldo Antonio de Souza <br />e Antonio Iondicy Castro Toledo</td><td class="date">1993</td></tr>
        <tr><td>José Rodrigo Vieira, <br />José Natalio Pereira <br />e João Pereira Bonfim</td><td class="date">1994</td></tr>
        <tr><td>José Rodrigues dos Santos Lobo</td><td class="date">1995</td></tr>
        <tr><td>Galvão Martins</td><td class="date">1996</td></tr>
        <tr><td>João Eugênio de Patablea</td><td class="date">1997</td></tr>
        <tr><td>Anesio Rodrigues da Silva, <br />José Benedito Almeida, <br />José Benedito da Silva (Zé Lemes) <br />e Geraldo Marcondes de Almeida (G. Jacinto)</td><td class="date">1998</td></tr>
        <tr><td>Padre Alaor dos Santos, <br />José Benedito da Silva (Zé Lemes), <br />Luis de Oliveira Coelho <br />e José Benedito de Almeida</td><td class="date">1999</td></tr>
        <tr><td>Alvaro Bonafé</td><td class="date">2000</td></tr>
        <tr><td>Sebastião Lobo dos Santos</td><td class="date">2001</td></tr>
        <tr><td>Mauricio Donizete dos Santos</td><td class="date">2002</td></tr>
        <tr><td>José Benedito de Almeida</td><td class="date">2003</td></tr>
        <tr><td>Antonio Carlos Bonafé</td><td class="date">2004</td></tr>
        <tr><td>Padre <br />e comissão</td><td class="date">2005</td></tr>
        <tr><td>Danilo José Toledo <br />e Família</td><td class="date">2006</td></tr>
        <tr><td>José Carlos Luzia Rodrigues</td><td class="date">2007</td></tr>
        <tr><td>José Borrielo Antunes de Andrade</td><td class="date">2008</td></tr>
        <tr><td>José Maria de Souza</td><td class="date">2009</td></tr>
        <tr><td>Antonio Galvão Salles</td><td class="date">2010</td></tr>
        <tr><td>José Arimatéia Cesar e José Orlando Santos</td><td class="date">2011</td></tr>
        <tr><td>Benedito Galvão Frade e família</td><td class="date">2012</td></tr>
        <tr><td>Família Rodrigues e Barbosa</td><td class="date">2013</td></tr>
        <tr><td>Alfredo Souza</td><td class="date">2014</td></tr>
        <tr><td>Garcia Dias e Valdo Rocha</td><td class="date">2015</td></tr>
        <tr><td>Antonio Fernandes</td><td class="date">2016</td></tr>
        <tr><td>José Arimatéia Cesar <br />e João Rafael Cursino dos Santos</td><td class="date">2017</td></tr>
        <tr><td>Patableo Tarcizo Rodrigues da Silva</td><td class="date">2018</td></tr>
        <tr><td>Ramon Barbosa Leite <br />e José Bertolino Moradei</td><td class="date">2019</td></tr>
        <tr><td>Luiz Claudio Cunha (Pé de Galo)</td><td class="date">2020</td></tr>
      </table>
      `
    }
  }
}

export default documents
