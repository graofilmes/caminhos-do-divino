import Vue from 'vue'
import App from '@/App.vue'
import router from '@/router'
import store from '@/store'

import VueAnalytics from 'vue-analytics'
import Vue2TouchEvents from 'vue2-touch-events'
import PerfectScrollbar from 'vue2-perfect-scrollbar'
import 'vue2-perfect-scrollbar/dist/vue2-perfect-scrollbar.css'
import panZoom from 'vue-panzoom'
import VuePannellum from 'vue-pannellum'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/css/swiper.css'

Vue.use(VueAnalytics, {
  id: 'UA-170163236-1',
  router
})
Vue.use(Vue2TouchEvents)
Vue.use(PerfectScrollbar)
Vue.use(panZoom)
Vue.component('VPannellum', VuePannellum)
Vue.use(VueAwesomeSwiper,
  {
    slidesPerView: 1,
    spaceBetween: 0,
    grabCursor: true,
    navigation: {
      nextEl: '.next',
      prevEl: '.prev'
    },
    pagination: {
      el: '.swiper-pagination',
      clickable: true
    }
  }
)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
